C> @file hexah22.f
C>    Two internal variables named
C>    - accumulated plastic strain denoted \f$p\f$
C>    - microstructure deviators  \f$\mathcal H=\f$

C> @attention TODO
C>    DDSDDE is not implemented, 

C-----------------------------------------------------------------------
C> @details 
C>    Desc and ref

C-----------------------------------------------------------------------
C> @param[in,out] SIG
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] D_EPS
C>    Total strain increment
C> @param[in] DT
C>    Time increment.

C> @param[in] params <b>(9)</b> 
C>    | symbol   | name                   | unit | default |
C>    |----------|------------------------|------|---------|
C>    | q        | general exponent       |  -   | 4.      |
C>    | rho_h    | micro dev  rate        |  -   | 0.      |
C>    | chi_max  | limit angle            | rad  | 0.314   |
C>    | rho_c    | cross load rate        |  -   | 0.      |
C>    | tau_c    | cross load target      |  -   | 0.7     |
C>    | gamma    | cross load expon       |  -   | 2.      |
C>    | rho_b    | Bauschinger rate       |  -   | 200.    |
C>    | tau_b    | Maximum truncation     |  -   | 0.7     |
C>    | beta     | Bauschinger expon      |  -   | 2.      |

C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.

C> @param[in,out] STATE_VARS  <b>(43)</b> REAL*8 array, dimension (NS)
C>    | Symbol   | Name                         |
C>    |----------|------------------------------|
C>    | acp      | accum plastic strain         |
C>    | p_a      | pointer to active deviator   |
C>    | p_l      | pointer to last deviator     |
C>    | h1_1     | microdev 1_1                 |
C>    | h1_2     | microdev 1_2                 |
C>    | h1_3     | microdev 1_3                 |
C>    | h1_4     | microdev 1_4                 |
C>    | h1_5     | microdev 1_5                 |
C>    | g_+1     | Bauschinger + 1              |
C>    | g_-1     | Bauschinger - 1              |
C>    | g_c1     | cross loading 1              |
C>    | h2_1     | microdev 2_1                 |
C>    | h2_2     | microdev 2_2                 |
C>    | h2_3     | microdev 2_3                 |
C>    | h2_4     | microdev 2_4                 |
C>    | h2_5     | microdev 2_5                 |
C>    | g_+2     | Bauschinger + 2              |
C>    | g_-2     | Bauschinger - 2              |
C>    | g_c2     | cross loading 2              |
C>    | h3_1     | microdev 3_1                 |
C>    | h3_2     | microdev 3_2                 |
C>    | h3_3     | microdev 3_3                 |
C>    | h3_4     | microdev 3_4                 |
C>    | h3_5     | microdev 3_5                 |
C>    | g_+3     | Bauschinger + 3              |
C>    | g_-3     | Bauschinger - 3              |
C>    | g_c3     | cross loading 3              |
C>    | h4_1     | microdev 4_1                 |
C>    | h4_2     | microdev 4_2                 |
C>    | h4_3     | microdev 4_3                 |
C>    | h4_4     | microdev 4_4                 |
C>    | h4_5     | microdev 4_5                 |
C>    | g_+4     | Bauschinger + 4              |
C>    | g_-4     | Bauschinger - 4              |
C>    | g_c4     | cross loading 4              |
C>    | h5_1     | microdev 5_1                 |
C>    | h5_2     | microdev 5_2                 |
C>    | h5_3     | microdev 5_3                 |
C>    | h5_4     | microdev 5_4                 |
C>    | h5_5     | microdev 5_5                 |
C>    | g_+5     | Bauschinger + 5              |
C>    | g_-5     | Bauschinger - 5              |
C>    | g_c5     | cross loading 5              |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/d_eps.
C> @param[out] INFO is INTEGER
C>    INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    INFO = -1 means unsuccesfull finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

C> @todo deal with info, test umat

      subroutine Return_Map(sig, d_eps, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
      implicit none  

      ! (in/out)
      integer  INFO, ns,
     +         np_HT, np_EL, np_ES, np_RS
      real*8   sig(6), d_eps(6), dt, 
     +         state_vars(ns), algmod(6,6),
     +         params(np_HT),
     +         par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)

      ! (internal)
      integer  i, i_max, j, j_max, k, l, p, pa, pl, pa_tmp
      
      real*8 ! State variables
     +  sig_dev(5), acp, GH(8,5), GH_0(8,5), GH_TMP(8,5), 
     +  d_sig_dev(5), d_acp, d_GH(8,5),  ! increments
     +  dd_sig_dev(5), dd_acp, dd_GH(8,5), ! iterates
     +  H_COEFFS(5)
      real*8 ! and derivatives
     +  sig_trial(5),
     +  d_eps_p(5), d_sig(6)                     ! others
      real*8  q, chi_max, !params
     +  sig_ref, eqs, grad(5), hess(5,5), dRdp ! yld fun
      real*8  !other
     +  dim_sig,
     +  choldiag(5), cholres(5),
     +  psi_0, psi, psi_tol, alpha, eta, beta, ! algo
     +  residuals(26),
     +  L_(5,5), Linv(5,5), C_inv(6,6), jac(26,26),
     +  dN_dacp(5), N_(5), tmp1, tmp15(15), tmp26(26)

      logical PRI, ISNAN

      !=================================================================
      ! SET PARAMS
      !-----------------------------------------------------------------

      PRI = .False.

      psi_tol = 1.d-20
      eta = 1.d-1
      beta = 1.d-4
      i_max = 50
      j_max = 0

      !=================================================================
      ! UNPACK STATEVARS/PARAMS
      !-----------------------------------------------------------------

      if (sum(state_vars).NE.sum(state_vars)) then
        write(*,*) "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        write(*,*) "@@ /!\ ULTIIMATE WARNING FROM HELL @@"
        write(*,*) "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        write(*,*) "NANS among state_vars"
        RETURN
      endif

      acp = state_vars(1)
      pa  = int(state_vars(2))
      pl  = int(state_vars(3))
      GH_0  = reshape( state_vars(4:43), (/8,5/) )! each column is 1 MD
      GH  = GH_0



      q = params(1)
      chi_max = params(3)
      !call print_statev(state_vars)
      !call print_GH(GH)

      !=================================================================
      ! TRIAL STRESS
      !-----------------------------------------------------------------

      ! Elastic guess
      call dStrain_to_dStress(d_eps, par_EL, d_sig)
      sig_trial = sig(2:6) + d_sig(2:6)

      d_GH = 0.d0
      d_acp = 0.d0
      d_eps_p = 0.d0!d_eps(2:6)

      ! Evaluate the yield function  f = eqs - sig_ref for trial stress
      call Reference_Stress(par_RS, acp, sig_ref)
      call EQS_AUGMENTED(sig_trial, GH, q, PAR_ES, np_ES, 1,eqs, grad)

      ! Tangent operators
      call Tangent_Compliance(par_EL, C_inv)
      call Tangent_Stiffness(par_EL, algmod)
      dim_sig = algmod(2,2)

      if (PRI) write(*,*) "YLD_CON: ", eqs, "<?<", sig_ref

      IF (eqs.LE.sig_ref) THEN

        alpha = 0.
        sig_dev = sig_trial

        d_GH = 0.d0
        residuals = 0.

        ! State_vars are unchanged
        ! Trial stress was good

        if (PRI) write(*,*) "ELASTIC STEP"

      !===============================================================
      ELSE  ! CLOSEST POINT PROJECTION
      !---------------------------------------------------------------

        ! Initialize newton iteration
        sig_dev = sig_trial/eqs*sig_ref
        d_sig_dev = sig_dev - sig(2:6)

        ! Initialize GH if necessary
        if (pa.eq.0) then
          pa = 1
          pl = 2
          GH(1:5,1) = sig_dev
          GH(1:5,1) = GH(1:5,1)/sum(GH(1:5,1)**2.)**.5
          GH(6:8,:) = 1.d0
          GH_0 = GH
        else
          call get_active_h(sig_dev, GH, chi_max, pa)! update pa with sig
        endif


        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)

        d_acp   = sum(d_eps_p*(sig_dev))/sig_ref
        acp = acp+d_acp

        d_GH = 0.
        dd_GH = 0.! needs it since we dont write on the entire thing

        if (PRI) write(*,*) "SIG_trial =", sig_trial
        if (PRI) write(*,*) "GUESS SIG_D =", sig_dev
        if (PRI) write(*,*) "GUESS d_ACP =", d_acp

        ! Compute residual
        call Reference_Stress(par_RS, acp, sig_ref)
        call dStress_to_dStrain_dev(sig_trial-sig_dev,
     +                              par_EL, d_eps_p)
        call EQS_AUGMENTED(sig_trial,GH,q,PAR_ES,np_ES,1,eqs,grad)
        call Residual(sig_dev, GH+d_GH, d_acp, d_eps_p, d_GH,  
     +    params, pa, dim_sig, eqs, grad, sig_ref,
     +    residuals(1),residuals(2:6),residuals(7:11),
     +    residuals(12:21),residuals(22:26),
     +    psi_0
     +  )

        if (PRI) write(*,*) "GUESS sig_ref =", sig_ref
        if (PRI) write(*,*) "PSI_0=", psi_0
        if (PRI) write(*,*) "RESIDUALS 2x13:"
        if (PRI) call print_array(reshape(residuals,(/2,13/)), 2,13)

        ! --------------------------------------------------------------
        ! Newton loop
        ! --------------------------------------------------------------        

        i = 0
        do while ((psi_0 .GT. psi_tol).and.(i .LE. i_max-1))
        !do while (i .LE. i_max-1)
          ! We enter the loop if psi_0 (initial guess) wasn't satisfactory
          i = i+1

c         ! HEXAH test update
c         GH_tmp = GH_0
c         pa_tmp = pa
c         call get_active_h(sig_dev, GH, chi_max, pa)! update pa with sig

c         ! Try to update GH and see if we must
c         if (pa_tmp.NE.pa) then
c           GH = GH_0
c           d_GH = 0.
c           dd_GH = 0.! needs it since we dont write on the entire thing
c           write(*,*) "CAAAAAAAAAAAAAAAAAAATCH"
c         call EQS_AUGMENTED(sig_dev+dd_sig_dev,GH+dd_GH,
c    +      q,PAR_ES,np_ES,1,eqs,grad)
c         call Residual(sig_dev+dd_sig_dev,GH+dd_GH,
c    +      d_acp+dd_acp,d_eps_p,d_GH+dd_GH,  
c    +      params, pa, dim_sig, eqs, grad, sig_ref,
c    +      residuals(1),residuals(2:6),residuals(7:11),
c    +      residuals(12:21),residuals(22:26),
c    +      psi
c    +    )
c         endif



          ! ............................................................
          ! NUMERICAL ! Inversion 

          call Reference_Stress(par_RS, acp, sig_ref)
          call Reference_Stress_Deriv(par_RS, acp, dRdp)
          call EQS_AUGMENTED_HESSIAN(
     +      sig_dev, GH, q, 
     +      PAR_ES, np_ES,
     +      hess
     +    )
          call Jacobian(
     +      sig_dev, GH, d_acp, d_GH, pa, !
     +      np_ES, par_ES, params,        !
     +      eqs, grad, hess,              !
     +      C_inv(2:6,2:6), dRdp,         !
     +      jac
     +    )   

          call linsolve(jac, tmp26, -residuals, 26)

          if (PRI) then
            write(*,*) "RESIDUALS 2x13"
            call print_array(reshape(residuals, (/2,13/)), 2,13)
            write(*,*) "JACOBIAN 6x6"
            call print_array(jac(1:6,1:6), 6, 6)
            write(*,*) "SOLUTION 2x13"
            call print_array(reshape(tmp26, (/2,13/)), 2,13)
            do i=1,26
              psi=0.
              do j=1,26
                psi = psi+jac(i,j)*tmp26(j)
              enddo
              write(*,*) psi, -residuals(i)
            enddo
          endif

          ! Update increments
          dd_acp        = tmp26(1)
          dd_sig_dev    = tmp26(2:6)
          dd_GH(1:5,pa) = tmp26(7:11)
          dd_GH(6,:)    = tmp26(12:16)
          dd_GH(7,:)    = tmp26(17:21)
          dd_GH(8,:)    = tmp26(22:26)

          ! ............................................................
          ! Residual (U+DU)
          call Reference_Stress(
     +      par_RS, acp+dd_acp, sig_ref)
          call dStress_to_dStrain_dev(
     +      sig_trial-sig_dev-dd_sig_dev,
     +      par_EL, d_eps_p)
          call EQS_AUGMENTED(sig_dev+dd_sig_dev,GH+dd_GH,
     +      q,PAR_ES,np_ES,1,eqs,grad)
          call Residual(sig_dev+dd_sig_dev,GH+dd_GH,
     +      d_acp+dd_acp,d_eps_p,d_GH+dd_GH,  
     +      params, pa, dim_sig, eqs, grad, sig_ref,
     +      residuals(1),residuals(2:6),residuals(7:11),
     +      residuals(12:21),residuals(22:26),
     +      psi
     +    )

           alpha = 1.d0
           ! ............................................................
           ! Line search 
           psi_0 = psi
           j = 0
c         do while ((psi.GT.(1.-2.*beta*alpha)*psi_0) 
c    +             .and. (j .LE. j_max))
c           ! entering this loop means alpha=1. is not satisfactory.
c           ! Let us adjust it and test over and over
c           j = j+1
c
c           alpha = max(eta*alpha, psi_0/(psi- psi_0*(1-2*alpha)))
c
c           !! Compute residual U+alpha*DU
c           call Reference_Stress(
c    +        par_RS, acp+alpha*dd_acp, sig_ref)
c           call dStress_to_dStrain_dev(
c    +        sig_trial-sig_dev-alpha*dd_sig_dev,
c    +        par_EL, d_eps_p)
c           call EQS_AUGMENTED(sig_dev+alpha*dd_sig_dev,GH+alpha*dd_GH,
c    +        q,PAR_ES,np_ES,1,eqs,grad)
c           call Residual(sig_dev+alpha*dd_sig_dev,GH+alpha*dd_GH,
c    +        d_acp+alpha*dd_acp,d_eps_p,d_GH+alpha*dd_GH,  
c    +        params, pa, dim_sig, eqs, grad, sig_ref,
c    +        residuals(1),residuals(2:6),residuals(7:11),
c    +        residuals(12:21),residuals(22:26),
c    +        psi
c    +      )
c         enddo

          ! ............................................................
          ! Final merit update

          if (PRI) write(*,
     +      "(A6,I3, A5,I2, A6,ES8.1, A5,F6.3,A4,ES8.1)")
c    +      "(A6,I3,A6,I3, 2ES8.1)", advance='no')
     +      "NEWT", i, 
     +      "| LS", j, 
     +      "| ψ0", psi_0,
     +      "| α", alpha,
     +      "| ψ", psi

          psi_0 = psi

          ! ------------------------------------------------------------
          ! Final state variables updates
          ! ------------------------------------------------------------

          d_sig_dev = d_sig_dev + alpha * dd_sig_dev
          d_acp     = d_acp     + alpha * dd_acp
          d_GH      = d_GH      + alpha * dd_GH

          sig_dev = sig_dev + alpha * dd_sig_dev
          acp     = acp     + alpha * dd_acp
          GH      = GH      + alpha * dd_GH

          d_sig(1) = alpha * d_sig(1)
          d_sig(2:6) = alpha * d_sig_dev

          do pl=1,5
            if (GH(6,pl).GT.1.d0) GH(6,pl) = 1.d0
            if (GH(7,pl).GT.1.d0) GH(7,pl) = 1.d0
            if (GH(8,pl).GT.1.d0) GH(8,pl) = 1.d0
            if (GH(6,pl).LT.params(8)) GH(6,pl) = params(8)
            if (GH(7,pl).LT.params(8)) GH(7,pl) = params(8)
            if (GH(8,pl).LT.params(5)) GH(8,pl) = params(5)
          enddo

        ! --------------------------------------------------------------
        ! End of NEWTON iteration
        ! --------------------------------------------------------------         NEWTON END
        enddo

        ! --------------------------------------------------------------
        ! Consistent tangent modulus
        ! --------------------------------------------------------------

        ! Get Linv then L
        CALL EQS_AUGMENTED_HESSIAN(
     +    sig_dev, GH, q, 
     +    PAR_ES, np_ES,
     +    hess
     +  )        

        Linv = C_inv(2:6,2:6) + d_acp*hess
        CALL INVERSE(Linv, 5, L_)

        dN_dacp = 0.
        ! We'll ignore the g and h dependencies in the algmod
        ! *Solution Technique, type=QUASI-NEWTON
        ! **Solution Technique, type=QUASI-NEWTON, reform kernel=10

        N_ = grad + dN_dacp*d_acp

        ! denominator
        call Reference_Stress_Deriv(par_RS, acp, dRdp)
        tmp1 = dRdp

        ! Get the 15 
        CALL dEQS_dg(sig_dev,GH, q,PAR_ES,np_ES, tmp15)
        do i=1, 5
          do j=1, 5
            algmod(1+i,1+j) = sum(L_(i,:)*N_) * sum(grad*L_(:,j))
            tmp1 = tmp1 - grad(i)*L_(i,j)*N_(j)
          enddo
        enddo
        algmod(2:,2:) = L_  + algmod(2:,2:)/tmp1


      ENDIF

      ! ================================================================
      ! Ultimate update
      ! ----------------------------------------------------------------

      if (pa.NE.0) then
      do i=1,5

        ! Make sure the gs are not too big
        if (GH(6,i).GT.1.d0) GH(6,i) = 1.d0
        if (GH(7,i).GT.1.d0) GH(7,i) = 1.d0
        if (GH(8,i).GT.1.d0) GH(8,i) = 1.d0
        
        ! Make sure the gs are not too small
        if (GH(6,i).LT.params(8)) GH(6,i) = params(8) ! TAU_B
        if (GH(7,i).LT.params(8)) GH(7,i) = params(8) ! TAU_B
        if (GH(8,i).LT.params(5)) GH(8,i) = params(5) ! TAU_C

        ! Normalize the Hs
        call normalize(GH(1:5,i),5)

        ! Write the Hs
        state_vars((8*(i-1)+4):(8*(i-1)+8)) = GH(1:5,i)
        ! Write the Gs
        state_vars((8*(i-1)+9):(8*(i-1)+11)) = GH(6:8,i)
      enddo
      endif

      sig(1) = sig(1)+d_sig(1)
      sig(2:6) = sig_dev

      state_vars(1)   = acp
      state_vars(2)   = real(pa)
      state_vars(3)   = real(pl)


      ! ================================================================
      ! Manual WARNINGS for NANs
      ! ----------------------------------------------------------------
      if (sum(residuals).NE.sum(residuals))write(*,*) "/!\ residuals"
      if (sum(sig).NE.sum(sig))       write(*,*) "/!\ sig"
      if (sum(algmod).NE.sum(algmod)) write(*,*) "/!\ algmod"
      if (acp.NE.acp)                 write(*,*) "/!\ acp"

      if (PRI) write(*,*) "YLD_CON_END:", eqs, "<?<", sig_ref
      
      RETURN

      end subroutine Return_Map





C#######################################################################



C-----------------------------------------------------------------------
C> @details Residual


      subroutine Residual(
     +  sig_dev, GH,           ! state vars
     +  d_acp, d_eps_p, d_GH,  ! their increment
     +  par_HT, pa, dim_sig,
     +  eqs, eqs_grad, sig_ref,
     +  Ryf, Rep, Rha, Rpm, Rcl,
     +  merit_fun
     +)
      ! Here we compute the total residual for each contribution except
      ! Rhi which is not useful since it amounts to 0-0 = 0

        implicit none

        ! IN/OUT
        real*8
     +    sig_dev(5), GH(8,5)
        real*8
     +    d_acp, d_eps_p(5), d_GH(8,5)
        integer
     +    pa
        real*8
     +    par_HT(9),
     +    eqs, eqs_grad(5),
     +    sig_ref, dim_sig
        real*8
     +    Ryf, Rep(5), Rha(5), Rpm(10), Rcl(5),
     +    merit_fun                 


        ! Internal
        integer i, i_start, i_end
        real*8 
     +    h_a(5), g_plus(5), g_minus(5), g_c(5),
     +    d_h(5), d_g_plus(5), d_g_minus(5), d_g_c(5),
     +    s_cos_chi, sig_hat(5), nu, d_h_dir(5)
        real*8
     +    rho_c, rho_b, tau_b, tau_c, exp_b, exp_c, 
     +    rho_h, tmp1

        ! Unpack =======================================================
        h_a =   GH(1:5,pa) ! the active one
        d_h = d_GH(1:5,pa)
        g_plus   =   GH(6,:)
        d_g_plus = d_GH(6,:)
        g_minus   =   GH(7,:)
        d_g_minus = d_GH(7,:)
        g_c   =   GH(8,:)
        d_g_c = d_GH(8,:)


        rho_h = PAR_HT(2)
        rho_c = PAR_HT(4)
        tau_c = PAR_HT(5)
        exp_c = PAR_HT(6)
        rho_b = PAR_HT(7)
        tau_b = PAR_HT(8)
        exp_b = PAR_HT(9)

        ! Prepings
        s_cos_chi = sum(sig_dev*h_a)
        nu = sign(1.d0, s_cos_chi) ! sign(a, b) = |a| (sign b)
        sig_hat = sig_dev/sqrt(sum(sig_dev*sig_dev))
        d_h_dir = nu*(sig_hat - sum(sig_hat*h_a)*h_a)

        ! ==============================================================
        ! Compute residuals individually 
        Ryf = eqs-sig_ref
        Rep = d_acp*eqs_grad - d_eps_p
        Rha = - d_h + d_acp * rho_h * d_h_dir
        Rpm(1:5) = -d_g_plus + d_acp*rho_b*(1.- g_plus)/ g_plus**exp_b
        Rpm(6:10)= -d_g_minus+ d_acp*rho_b*(1.-g_minus)/g_minus**exp_b
        Rcl = -d_g_c + d_acp*rho_c*(1.-g_c)/g_c**exp_c
        ! Fix for the active one
        if (nu.LT.0.d0) then
          Rpm(pa)  = -d_g_plus(pa)
     +      +d_acp*rho_b*( TAU_B  - g_plus(pa) )/g_plus(pa) **exp_b
        else
          Rpm(5+pa)=-d_g_minus(pa) 
     +      +d_acp*rho_b*( TAU_B - g_minus(pa) )/g_minus(pa) **exp_b
        endif
        Rcl(pa) = -d_g_c(pa) 
     +    +d_acp*rho_c*(  TAU_C  -g_c(pa) )/g_c(pa) **exp_c


        ! ==============================================================
        ! Compute The merit function with normalized residuals
        merit_fun  = (
     +    (Ryf**2.)/dim_sig**2 +
     +    sum(Rep**2.) + sum(Rha**2.) + sum(Rpm**2.) + sum(Rcl**2.)
     +  )



        tmp1 =  sum(Rep) + sum(Rha) + sum(Rpm) + sum(Rcl) + Ryf

        !if (merit_fun.NE.merit_fun) then
        if (tmp1.NE.tmp1) then
          write(*,*) "///!!!\\\ psi-ngular"
          !write(*,*) "RYF =", RYF
          !write(*,*) "REP =", REP
          !write(*,*) "RHA =", RHA
          !write(*,*) "RPM =", RPM
          !write(*,*) "RCL =", RCL
          !write(*,*) "sigd =", SIG_DEV
        endif

        RETURN
      end subroutine Residual

C> @details Jacobian
      subroutine Jacobian(
     +  sig_dev, GH,
     +  d_acp, d_GH,
     +  pa,
     +  np_ES, par_ES, par_HT, 
     +  eqs, eqs_grad, eqs_hess,
     +  C_inv,
     +  dsigref_dacp,
     +  jac
     +)                  
        implicit none
        ! IN/OUT
        real*8
     +    sig_dev(5), GH(8,5),
     +    d_acp, d_GH(8,5)
        integer
     +    pa, np_ES
        real*8
     +    par_HT(9), par_ES(np_ES),
     +    eqs, eqs_grad(5), eqs_hess(5,5),
     +    C_inv(5,5),
     +    dsigref_dacp
        real*8
     +    jac(26,26)

        ! Internal
        integer i,j
        real*8 h_a(5),   g_plus(5),  g_minus(5),  g_c(5),
     +         d_h(5), d_g_plus(5),d_g_minus(5),d_g_c(5)
        real*8 s_cos_chi, sig_hat(5), nu
        real*8 q, rho_c, rho_b, tau_b, tau_c, exp_b, exp_c, rho_h
        real*8 dRg_dg
        real*8 phi_pca, f_nua
        real*8 tmp5(5), tmp55(5,5), GH_TMP(8,5), tmp15(15)

        ! Unpack =======================================================
        h_a =   GH(1:5,pa) ! the active one
        d_h = d_GH(1:5,pa)
        g_plus   =   GH(6,:)
        d_g_plus = d_GH(6,:)
        g_minus   =   GH(7,:)
        d_g_minus = d_GH(7,:)
        g_c   =   GH(8,:)
        d_g_c = d_GH(8,:)

        q = PAR_HT(1)
        rho_h = PAR_HT(2)
        rho_c = PAR_HT(4)
        tau_c = PAR_HT(5)
        exp_c = PAR_HT(6)
        rho_b = PAR_HT(7)
        tau_b = PAR_HT(8)
        exp_b = PAR_HT(9)

        ! Prepings =====================================================
        s_cos_chi = sum(sig_dev*h_a)
        nu = sign(1.d0, s_cos_chi) ! sign(a, b) = |a| (sign b)
        sig_hat = sig_dev/sqrt(sum(sig_dev*sig_dev))
        !Rha = - d_h + d_acp * rho_h * d_h_dir
        call Equivalent_Stress(
     +      (sig_dev - sum(sig_dev*h_a)*h_a)*(4-4*g_c(pa)) ,
     +      par_ES, 0, phi_pca, tmp5, tmp55)
        if (s_cos_chi.GE.0) then
          f_nua = 1.5d0**0.5d0 * (g_plus(pa)**(-q) - 1.d0)
        else
          f_nua = 1.5d0**0.5d0 * (g_minus(pa)**(-q) - 1.d0)
        endif


        ! INITIALIZE
        jac = 0.

        ! Ryf, the yield function residual _____________________________
        jac(1, 1   ) = -dsigref_dacp
        jac(1, 2: 6) = eqs_grad
        ! \partial\bar\sigma / \partial h_active........................
        jac(1, 7:11) = 
     +    (h_a*eqs - eqs_grad*s_cos_chi) * 4 * (1-g_c(pa)) 
     +    * (phi_pca/eqs)**(q-1.d0)
     +    + sig_dev * f_nua
     +    * (f_nua*nu*s_cos_chi/eqs)**(q-1.d0)
        ! \partial\bar\sigma / \partial g ..............................
        CALL dEQS_dg(sig_dev,GH, q,PAR_ES,np_ES, tmp15)
        !write(*,*) sum(tmp15)
        !call print_array(tmp15, 1, 15)
        jac(1,12:26) = tmp15

        ! Rep, the elastic residual ____________________________________
        jac(2:6, 1   ) = eqs_grad
        jac(2:6, 2: 6) = C_inv + eqs_hess*d_acp

        ! Rmd, the microstructure residual _____________________________
        jac(7:11, 1   ) = rho_h * nu*(sig_hat - sum(sig_hat*h_a)*h_a)
        !                         \_______ d_h_dir above ___________/
        ! Rh,sig
        do i=1,5 
          do j=1,5
            !old jac(6+i, 1+j) = nu * d_acp*rho_h*(1.d0-h_a(i)*h_a(j))
            jac(6+i, 1+j) = nu * d_acp*rho_h*( 1.d0
     +        -     h_a(i)*h_a(j)           ! this is the contraction 
     +        - sig_hat(i)*sig_hat(j)       ! of the two
     +        + sig_hat(i)*h_a(j)*s_cos_chi ! ortho projectors
     +        ) / (sum(sig_dev*sig_dev)**.5d0)
          enddo
        enddo
        ! Rh,h
        do i=1,5 
          jac(6+i,6+i) = -1.d0 - nu * d_acp*rho_h* sum(sig_hat*h_a)
          do j=1,5
            jac(6+i,6+j) = jac(6+i,6+j) - nu * d_acp*rho_h*(
     +        sig_hat(i)*h_a(j)
     +      )
          enddo
        enddo

        ! Rpm, the truncation __________________________________________
        jac(12:16, 1   ) = rho_b*(1.-g_plus)/g_plus**exp_b
        jac(17:21, 1   ) = rho_b*(1.-g_minus)/g_minus**exp_b
        do i=1,5
          jac(11+i,11+i) = dRg_dg( g_plus(i), d_acp, rho_b, 1.d0, exp_b)
          jac(16+i,16+i) = dRg_dg(g_minus(i), d_acp, rho_b, 1.d0, exp_b)
        enddo
        ! Fix for the active one
        if (nu.LT.0.d0) then
          jac(11+pa, 1) = rho_b*(tau_b-g_plus(pa))/g_plus(pa)**exp_b
          jac(11+pa,11+pa) =
     +      dRg_dg( g_plus(pa), d_acp, rho_b, tau_b, exp_b)
        else
          jac(16+pa, 1) = rho_b*(tau_b-g_minus(pa))/g_minus(pa)**exp_b
          jac(16+pa,16+pa) =
     +      dRg_dg(g_minus(pa), d_acp, rho_b, tau_b, exp_b)
        endif

        ! Rc, the cross load ___________________________________________
        jac(22:26,1    ) = rho_c*(1.-g_c)/g_c**exp_c
        do i=1,5
          jac(21+i,21+i) = dRg_dg(g_c(i), d_acp, rho_c, 1.d0, exp_c)
        enddo
        ! Fix for the active one
        jac(21+pa,1) = rho_c*(tau_c-g_c(pa))/g_c(pa)**exp_c
        jac(16+pa,16+pa) =
     +    dRg_dg(g_c(pa), d_acp, rho_c, tau_c, exp_c)

        if (sum(jac).NE.sum(jac)) then
          write(*,*) "//!!\\ JAAAAAAAAAAAAAAAAAAAAC in jacobian"
          write(*,*) "       ----------------------", sum(jac)
          !call print_array(jac(1:6,1:6), 6, 6)
          write(*,*) eqs, phi_pca, f_nua, q
          write(*,*) pa, g_plus(pa), g_minus(pa), s_cos_chi
          !do i=1,26
          !  do j=1,26
          !    !write(*,*) i, j, jac(i,j)
          !    if ((1.+jac(i,j)).NE.(1.+jac(i,j))) jac(i,j) = 0.
          !  enddo
          !enddo
        endif

        RETURN
      end subroutine Jacobian


C> @details helper fct
      real*8 function dRg_dg(g_, d_acp, rho, targ, expon)
        implicit none 
        real*8 g_, d_acp, rho, targ, expon
        dRg_dg = -1.d0 + d_acp*rho*(
     +    - expon * targ * g_**(expon-1.d0)
     +    - (1.d0-expon) * g_**(-expon)
     +  )
        return
      end

C> @details helper fct
      subroutine dEQS_dg(sig_dev,GH, q,PAR_ES,np_ES, DFDG)
        implicit none
        integer np_ES
        real*8 GH(8,5), sig_dev(5), q, PAR_ES(np_es), DFDG(15)

        integer i, j
        real*8 g_tmp, GH_TMP(8,5), DG, eqs,
     +         tmp1, tmp5(5),
     +         dum1, dum5(5)

        DG = 1.d-30 ! Parametrizable here

        GH_TMP = GH
        do i=1, 5
          do j=1,3
            GH_TMP(5+j, i) = GH(5+j, i) + DG
            call EQS_AUGMENTED(
     +        sig_dev, GH_TMP,
     +        q,PAR_ES,np_ES,1, eqs,    dum5
     +      )
            GH_TMP(5+j, i) = GH(5+j, i) - DG
            call EQS_AUGMENTED(
     +        sig_dev, GH_TMP,
     +        q,PAR_ES,np_ES,1, tmp1, dum5
     +      )
            DFDG((j-1)*5+i) = (eqs - tmp1)/2.d0/DG
            GH_TMP(5+j, i) = GH(5+j, i)
          end do
        end do

        RETURN

      end subroutine

C-----------------------------------------------------------------------
C> @details helper fct
      subroutine EQS_AUGMENTED(
     +    sig, GH, q, 
     +    PAR_ES, np_ES, mode,
     +    eqs, grad
     +  )
        implicit none

        ! i/o
        integer mode, np_ES
        real*8 sig(5), GH(8,5), par_ES(np_ES), q
        ! other: EQS
        integer i
        real*8 eqs
        real*8 phi, phi_perp_q, phi_trunc_q !
        real*8 g_c, g_pm, h(5)                ! state vars
        real*8 s_cos_chi                         !
        real*8 tmp, tmp55(5,5)
        ! other: D_EQS
        real*8 grad(5), d_phi(5), d_phi_perp(5), d_phi_trunc(5)
        real*8 phi_perp, phi_trunc !
        real*8 nu

      !=================================================================
      ! Initialize
      !=================================================================

        eqs = 0.d0
        phi = 0.d0
        phi_perp_q = 0.d0
        phi_trunc_q = 0.d0

      !=================================================================
      ! Loop for each of the five contributions
      !=================================================================
        do i=1,5
          ! Grab the Micro deviator.....................................
          h       = GH(1:5,i)
          !skip if that h has not been instantiated (small norm)
          IF (sum(h*h).LT.0.5d0) CYCLE 

          ! Compute Bauschinger contribution ...........................
          s_cos_chi = sum(h*sig)                                       !
          if (s_cos_chi.GE.0) then                                     !
            g_pm  = GH(6,i)! case: G_PLUS                              !
          else                                                         !
            g_pm  = GH(7,i)                                            !
          endif                                                        !
          if (g_pm.GT.0.d0) then 
            phi_trunc_q = phi_trunc_q                                  !
     +        + sqrt(1.5)*(min(1.d0, g_pm)**(-q)-1)  !---> f_pm
     +        * abs(s_cos_chi)**q         ! <(+-) sig:h> 
          endif

          ! Compute cross load contribution  ...........................
          g_c = GH(8,i)                                                !  
          call Equivalent_Stress(                                      !
     +      (sig - sum(sig*h)*h)*(4-4*g_c) ,                           !
     +      par_ES, 0,
     +      tmp, grad, tmp55)

          phi_perp_q = phi_perp_q + tmp**q
          d_phi_perp = d_phi_perp + (grad - sum(grad*h)*h)*(4-4*g_c)
          !
        enddo
        ! Compute base                 .................................
        call Equivalent_Stress(                                        !
     +    sig,                                                         !
     +    par_ES, 1,                                                   !
     +    phi, grad, tmp55)

        ! Conclude .....................................................
        eqs = (phi**q + phi_perp_q + phi_trunc_q)**(1.d0/q)


      !=================================================================
        IF (mode.EQ.0) RETURN!==========================================
      !=================================================================
      ! We must redo the entire loop anyways because we need the final
      ! value of every contribution.

        phi_perp = phi_perp_q**(1.d0/q)
        phi_trunc = phi_trunc_q**(1.d0/q)

        d_phi       = 0.
        d_phi_trunc = 0.
        d_phi_perp  = 0.

        !grad = sig/phi*1.5
        do i=1,5
          ! Grab the Micro deviator.....................................
          h       = GH(1:5,i)
          !skip if that h has not been instantiated (small norm)
          if (sum(h*h).LT.0.5d0) cycle 

          ! Compute Bauschinger contribution ...........................
          if (phi_trunc.GT.0.d0) then
            s_cos_chi = sum(h*sig)                                       !
            if (s_cos_chi.GE.0) then                                     !
              g_pm  = GH(6,i)! case: G_PLUS                              !
              nu = 1.d0
            else                                                         !
              g_pm  = GH(7,i)                                            !
              nu = -1.d0
            endif                                                        !
            if (g_pm.GT.0.d0) then 
              d_phi_trunc = d_phi_trunc
     +          + sqrt(1.5)*(g_pm**(-q)-1.d0)        ! f^q
     +          * abs(s_cos_chi/phi_trunc)**(q-1.d0)           ! 
     +          * nu * abs(s_cos_chi) * h           ! 
            endif
          endif

          ! Compute cross load contribution  ...........................
          ! At this point grad is the base one, so we just need to
          ! project it on Pc
          g_c = GH(8,i)                                                
          call Equivalent_Stress(                                     
     +      (sig - sum(sig*h)*h)*(4-4*g_c) ,                         
     +      par_ES, 0,
     +      tmp, grad, tmp55)                                        
          ! tmp yield phi_c_i
          if (phi_perp.GT.0.d0) then
            d_phi_perp = d_phi_perp 
     +               + (grad - sum(grad*h)*h)*(4-4*g_c)*(phi_perp)
     +               * (tmp/phi_perp)**(q-1.d0)
          endif

        enddo

        grad =
     +    + phi**(q-1.d0) * grad!d_phi
     +    + phi_perp**(q-1.d0) * d_phi_perp
     +    + phi_trunc**(q-1.d0) * d_phi_trunc


        if (eqs.GT.0.d0) grad = grad/(eqs**(q-1.d0))


      !=================================================================
        IF (mode.EQ.1) RETURN!==========================================
      !=================================================================

        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"
        write(*,*) "/!\ WARNIIIIIIIIIIIIIIIING, hessian requested"
        write(*,*) "/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\"

        RETURN
      end subroutine EQS_AUGMENTED
      
C> @details helper fct
      subroutine EQS_AUGMENTED_HESSIAN(
     +    sig, GH, q, 
     +    PAR_ES, np_ES,
     +    hess
     +  )
        implicit none
        ! i/o
        integer np_ES
        real*8 sig(5), GH(8,5), par_ES(np_ES), q
        real*8 hess(5,5)

        ! internal
        integer i, j
        real*8 d_size, d_sig(5)
        real*8 grad(5)
        real*8 dummy1


        ! We shall compute the gradient derivative numerically, with a
        ! simple mid-point rule
        ! This means 5*5 * 2 calls to the evaluation function

        !---------------------------------------------------------------
        ! Initialize
        d_size = 1.d-10
        !d_sig  = 0.d0

        !---------------------------------------------------------------
        ! Compute each derivative individually, numerically
        do j=1,5
          d_sig = 0.d0
          d_sig(j) = d_size
          do i=1,5
            call EQS_AUGMENTED(sig+d_sig, GH, q,
     +                        PAR_ES, np_ES, 1, dummy1, grad)
            hess(i,j) = grad(i)
            call EQS_AUGMENTED(sig-d_sig, GH, q, 
     +                         PAR_ES, np_ES, 1, dummy1, grad)
            hess(i,j) = ( hess(i,j) - grad(i) )/2/d_size
          enddo
        enddo

        if (sum(hess).NE.sum(hess)) write (*,*) "//!!\\ hess in hessian"

        RETURN

      end subroutine EQS_AUGMENTED_HESSIAN

C> @details helper fct
      subroutine print_statev(statev)
        implicit none
        REAL*8 statev(43), num
        CHARACTER(LEN=101) FMT
        INTEGER i, j

        FMT = "(A12, 5ES12.1)"
        write(*,FMT) "ACP:", statev(1)
        write(*,FMT) "PA:", statev(2)
        write(*,FMT, advance="no") "PL:", statev(3)
        write(*,"(A51,3A12)") "--------------------| ", "g+", "g-", "gc"
        do i=1, 5
          !write(*,FMT) "microdev:", statev(4+8*(i-1):8+8*(i-1))
          !write(*,"(A12)", advance="no") "microdev:"
          write(*,"(A10,I2)", advance="no") "contrib", i
          do j=1,5
            call print_real(statev((3+j)+8*(i-1)))
          enddo
          write(*,"(A3)", advance="no") " | "
          do j=1,3
            call print_real(statev((8+j)+8*(i-1)))
          enddo
          write(*,*)
        enddo
      end subroutine print_statev

c     subroutine print_real(num)
c       implicit none
c       real*8 num
c       if  (abs(num).LT.1.d-100) then
c         write(*,"(A12)", advance="no") "-"
c       else
c         write(*,"(ES12.3)", advance="no") num
c       endif
c     end subroutine print_real

C> @details helper fct
      subroutine print_gh(gh)
        implicit none
        REAL*8 GH(8,5), num
        CHARACTER(LEN=101) FMT
        INTEGER i, j

        write(*,"(A75,3A12)") "____________________| ", "g+", "g-", "gc"
        do i=1, 5
          write(*,"(A10,I2)", advance="no") "contrib", i
          do j=1,5
            call print_real(GH(j,i))
          enddo
          write(*,"(A3)", advance="no") " | "
          do j=6,8
            call print_real(GH(j,i))
          enddo
          write(*,*)
        enddo
      end subroutine print_gh

c     subroutine print_array(arr, a, b)
c       implicit none
c       integer a, b, i, j
c       real*8 arr(a,b)

c       do i=1,a
c         do j=1, b
c           call print_real(arr(i,j))
c         enddo
c         write(*,*)
c       enddo
c     end subroutine print_array

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C> @details helper fct
      subroutine get_active_h(sig_dev, GH, chi_max, pa)
        implicit none
        integer pa
        real*8 GH(8,5), sig_dev(5), chi_max

        integer i, pl
        real*8 cos_chi, cos_chi_closest, dir(5), coeffs(5)

        dir = sig_dev/sum(sig_dev*sig_dev)**.5d0
        cos_chi_closest = -2.d0 !\ so that any value will replace them

        ! Find whichever contribution is closest to the stress
        ! Assign pa 
        do i=1,5
          cos_chi = abs(sum(GH(1:5,i)*dir))
          if (cos_chi.GT.cos_chi_closest) then
            pa = i
            cos_chi_closest = cos_chi
          endif
        enddo

        ! Knowing pa, make sure the corresponding cos is not too small
        ! Otherwise spawn new one at pl.
        if (cos_chi_closest.LT.abs(cos(chi_max))) then
          ! find pl
          do i=1,5
            coeffs(i) = sum( (1.-GH(6:8,i))**2.d0 )
          enddo
          pl = MINLOC(coeffs, 1)
          ! Assign pa
          pa=pl
          GH(1:5,pa) = dir
c         write(*,*) "Spawned new h at pa=", pa
c    +      ,"because", cos_chi_closest, ".LT.", abs(cos(chi_max))
        endif

C        IF (sum(GH(1:5,pa)).NE.sum(GH(1:5,pa))) THEN
C          GH(1:5,pa) = 0.
C          GH(1,pa) = 1.
C          call print_GH(GH)
C          write(*,*) "SIGGGG_D =", sig_dev
C        ENDIF

        ! No need to store pl since it must be computed every time

        RETURN

      end subroutine get_active_h

