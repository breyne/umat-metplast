#!/bin/bash
# Run case with activated guess and LS, then scale the guess

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

mkdir -p dat
mkdir -p dat/guess_scale
mkdir -p dat/PGF_DATA

casename="multi_dir.F"
umockname="ZieglerPrager.F" #"Holmedal.F" #"Isotropic.F" #

gfortran -cpp cases/${casename} -c -o tmp_case.o

for thescale in $(seq 0.1 0.1 2.0); do
 echo $thescale
  basename="scale_${casename:0:3}_${umockname:0:3}_$thescale"
  basename="${basename,,}"
  if [ -f "./dat/guess_scale/${basename}.log" ]; then
   echo "(EXISTS) $basename"
  else
   gfortran -cpp -DLS_MAX=50 -DRES_SIZE=1.E-15  \
    -DBEST_FIRST_GUESS -DGUESS_SCALE=$thescale tmp_case.o umocks/$umockname #G1S1
   ./a.out > ./dat/guess_scale/${basename}.log
  fi
  csv_args=$csv_args" ./dat/compare/guess_scale/${basename}.log"
done

for file in ./dat/guess_scale/*.log; do
 echo ${file%.*}_STUFF.csv
 cat $file | grep "MAIN"        | cut -c7-  > ${file%.*}_variables.csv
 cat $file | grep "CONVERGENCE" | cut -c14- > ${file%.*}_convergence.csv
done

#python analysis/csv_plot.py 0 1 $csv_args
