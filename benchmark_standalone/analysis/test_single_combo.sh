# Run a single combination of CASE and UMOCK

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

mkdir -p dat

if [ "$#" -eq 2 ]; then
  echo "====================================="
  echo case $1
  echo umock $2
  echo "-------------------------------------"
else
  echo "ARG_ERROR: arguments should be <casename> <umockname>"
  exit 666
fi

gfortran -cpp cases/${1}.F umocks/${2}.F
  
./a.out > dat/${1}_${2}.log

cat dat/${1}_${2}.log | grep "MAIN" | cut -c7- > dat/${1}_${2}_variables.csv
cat dat/${1}_${2}.log | grep "CONVERGENCE" | cut -c14- > dat/${1}_${2}_convergence.csv

python analysis/csv_plot.py 0 1 dat/${1}_${2}_variables.csv
echo "====================================="
