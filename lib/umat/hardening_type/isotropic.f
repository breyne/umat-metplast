C> @file hardening_type/isotropic.f Isotropic hardening.
C>    One internal variable named accumulated
C>    plastic strain and denoted \f$p\f$.

C> @attention \e Isotropic designates the expansion of
C>    the yield surface. The initial yield surface can be anisotropic.
C>    

C-----------------------------------------------------------------------
C> @details 
C>    This is the return mapping predictor-corrector algorithm for 
C>    solving the closest-point projection in an elastic-plastic 
C>    problem. It is based on a Newton-Raphson method with 
C>    line-search.
C>    The algorithm is based on the paper by Scherzinger, W. M.
C>    @cite scherzinger2017.
C>    This particular implementation is for isotropic elasticity, 
C>    isotropic Voce hardening and Yld2004-18p yield surface.
C>    The implementatoin is detailed by Manik et al. @cite manik2021.

C-----------------------------------------------------------------------
C> @param[in,out] svec
C>    Cauchy stress at the beginning of the time 
C>    increment expressed in the natural notation. At return it is 
C>    updated by the subroutine to be the stress at the end of the
C>    time increment.
C> @param[in] dstrain
C>    Total strain increment, natural basis
C> @param[in] DT
C>    Time increment.
C> @param[in] params <b>(0)</b> The Hardening Type parameters: NONE.
C> @param[in] NP_HT is INTEGER
C>    It defines the length of array PAR_HT.

C> @param[in] PAR_EL The elastic parameters
C> @param[in] NP_EL Their amount
C> @param[in] PAR_ES The Equivalent Stress parameters
C> @param[in] NP_ES The length of array PAR_ES.

C> @param[in] PAR_RS is REAL*8 array, dimension (NP_RS)
C>    The Reference_Stress parameters
C> @param[in] NP_RS is INTEGER
C>    It defines the length of array PAR_RS.
C> @param[in,out] STATE_VARS  <b>(7)</b> REAL*8 array, dimension (NS)
C>    It contains the solution-dependent state variables as e.g.
C>    equivalent plastic strain, hardening model variables,
C>    algorithm convergence info
C>    | Symbol | Name                       |
C>    |--------|----------------------------|
C>    |  acp   | accumulated plastic strain |
C>    | dlamb  | plastic multiplier         |
C>    | r      | /!\ not saved              |
C>    | Imax   | newton iterations used     |
C>    | Jmax   | linesearch iterations used |
C>    | INFO   | convergence information    |
C>    | psi    | final residual value       |

C>
C> @param[in] NS is INTEGER
C>    It defines the length of array STATE_VARS.
C> @param[out] algmod is REAL*8 array, dimension (6,6)
C>    It is the algorithmic modulus defined as dstress/dstrain.
C> @param[out] INFO is INTEGER
C>    INFO = 0 means that the algorithm finished successfully with
C>    use of the Newton-Raphson steps only, i.e. no need for 
C>    line-seach
C>    INFO = 1 means that the algorithm finished successfully and
C>    the line-search was activated at least once
C>    INFO = 2 means that the algorithm finished successfully 
C>    within IMAX iterations but there was at least one 
C>    iteration in which line-search was terminated due to 
C>    reaching JMAX line-search iterations
C>    INFO = 3 means that there was no need for plastic corrector
C>    since the yield function at STRIAL <= 0.d0
C>    INFO = 4 means that there was no need for plastic corrector
C>    since the initial return-map guess solved the residuals
C>    INFO = -1 means unsuccesfull finish, i.e. the residual PSI
C>    did not get below TOL within IMAX iterations

      subroutine Return_Map(svec, dstrain, dt,
     +                      state_vars, ns,
     +                      params, np_HT, 
     +                      par_EL, np_EL,
     +                      par_ES, np_ES,
     +                      par_RS, np_RS,
     +                      algmod, INFO )
c**********************************************************************

c**********************************************************************
      implicit none  

c --- in/out
      integer     info, ns, !np 
     +            np_HT, np_EL, np_ES, np_RS

      real*8      svec(6), dstrain(6), dt, 
     +            state_vars(ns), algmod(6,6),
     +            params(np_HT),
     +            par_EL(np_EL), par_ES(np_ES), par_RS(np_RS)
      logical     doprint

c --- internal
      integer     i, j, k, iter, imax, jmax,jlargest
      real*8      Dp(5), DpI(5), res(5), strial(5), f, grad(5),
     +            hess(5,5), alpha, psi0, psi, lamdot, 
     +            Linv(5,5), a, L(5,5), beta,
     +            dlamdot, eta, dsdev(5), lamdotI, sdevI(5), p(5),
     +            sy, vdot, y(5), y2(5), sdev(5),
     +            H, choldiag(5), denom, hard,
     +            tol,
     +            dStress(6),dStDev(5),Cinv(6,6),! elastic generalization
     +            ep, epI, depI
c    +            Emod, nu, shearmod2, bulkmod3, 
      logical     linesearch
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      real*8      factor
c ---------------------- DEFINING SOME CONSTANTS ----------------------
      ov2 = 0.5d0
      ov3 = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt2 = 1.d0/ovsqrt2
      sqrt3 = 1.d0/ovsqrt3

C --- FORMERLY IN UMAT main --------------------------------------------
c     defining tolerance for the return-map algorithm
      tol = 1.d-18
c     disable controlled printing of the iteration process
      doprint = .FALSE.
c     stress initial guess factor 
      factor = 1.0d0

c     Newton-Raphson and line-search parameters ------------------------
      beta = 1.d-4
      eta  = 0.1d0
      jmax = 10
      imax = 200
      linesearch = .false.
      jlargest = 0
C=======================================================================
c --- SOLUTION-DEPENDENT STATE VARIABLES FROM THE LAST STEP -------
c     
c     equivalent plastic strain
      ep = state_vars(1)
c     equivalent plastic strain rate (no need to read)
c     state_vars(2)
c     isotropic hardening
CBAPTIR = state_vars(3)
c     number of iterations used (no need to read)
c     state_vars(4)
c     max number of line-searches for one Newton iter
c     state_vars(5)
c     convergence INFO
c     state_vars(6)
c     final value of PSI residual (no need to read)
c     state_vars(7)
c
C=======================================================================
c     yield stress
      call Reference_Stress(par_RS, ep, sy)
c     isotorpic hardening modulus
      call Reference_Stress_Deriv(par_RS, ep, hard)
c
c --- ELASTIC PREDICTOR -------
c

c     hydrostatic pressure update
      call dStrain_to_dStress(dStrain, par_EL, dStress)
      svec(1) = svec(1) + dStress(1)

      do i=1, 5
c         compute deviatoric trial stress
          strial(i) = svec(i+1) + dStress(i+1)
      end do
c
c     yield function at STRIAL
      call Equivalent_Stress(strial, par_ES, 0, f, grad, hess)
      call Tangent_Stiffness(par_EL, algmod)
      call Tangent_Compliance(par_EL, Cinv)
c
c     if no need for the plastic corrector
      if ((f-sy) .LT. 0.d0) then
c         update the stress
          do i=1, 5 
              svec(i+1) = strial(i)
          end do
c         caluclate the algorithmic modulus
c         solution flag
          INFO = 3
c         update the state variables after pure elastic step
c         state_vars(1) = no need to update
          state_vars(2) = 0.d0
c         state_vars(3) = no need to update
          state_vars(4) = 0.d0
          state_vars(5) = 0.d0
          state_vars(6) = real(INFO)
          state_vars(7) = 0.d0
          goto 101
      end if
c
c --- PLASTIC CORRECTOR -------
c
c     making the initial guess
      sdev = strial*sy/f*factor
      dStdev = (strial-sdev)/dt
      call dStress_to_dStrain_dev(dStdev, par_EL, Dp)
c     initial guess for plastic multiplier
      lamdot = vdot(Dp, sdev, 5)/sy
C=======================================================================
c     initial guess for the hardening stress R
      call Reference_Stress(par_RS, ep + lamdot*dt, sy)  

c     compute residuals
      call Equivalent_Stress(sdev, par_ES, 1, f, grad, hess)
c
      !do i=1, 5
      !    res(i) = -Dp(i) + lamdot*grad(i)
      !end do
      res = lamdot*grad -Dp 

c     calculate the merit function
      psi0 = ov2*(vdot(res,res,5)*dt**2 + ((f-sy)/algmod(2,2))**2)
c
      if (doprint) write(*, *) 'Psi initial ', psi0
c
c     case when initial radial return guess solves the residuals
      if (psi0 .LT. tol) then
          psi = psi0
          INFO = 4
          iter = 0
          goto 102
      end if
c
c     start the iterations
      depI = lamdot*dt
      do iter=1, imax
c
          if (doprint) write(*, *) 'Iter no. ', iter
c
c         start with the Newton step, i.e. alpha = 1
          alpha = 1.d0
c         calculate yield func, its gradient and hessian
          call Equivalent_Stress(sdev, par_ES, 2, f, grad, hess)
c         due to major symmetry of both elastic modulus and
c         the Hessian, only the upper triangle is calculated
          call Tangent_Compliance(par_EL, Cinv)
          do i=1, 5
            do k=i, 5
              Linv(i,k) = Cinv(i+1,k+1)/dt + lamdot*hess(i,k)
            end do
          end do    
c         solve Linv*y = grad by Cholesky decomposition, i.e.
c         y = L*grad
c         (lower triangle of Linv will be modified but the upper
c          triangle incl. the diagonal will not)
          call chol_decomp(Linv, 5, choldiag)
          call chol_solve(Linv, 5, grad, choldiag, y)
C=======================================================================
c         plastic modulus H
          call Reference_Stress_Deriv(par_RS, ep + depI, H)
          H = H*dt
c         yield stress
          call Reference_Stress(par_RS, ep + depI, sy)
c         calculate plastic multiplier increment
          denom = vdot(grad,y,5) + H
          dlamdot = ((f-sy) - vdot(res,y,5))/denom
c         
          y2 = res + dlamdot*grad
          

c         solve Linv*dsdev = y2 by Cholesky decomposition
          call chol_solve(Linv, 5, y2, choldiag, dsdev)

          dsdev = -dsdev
          sdevI = sdev + dsdev
          dStdev = (strial-sdevI)/dt
c         update plastic strain rate
          call dStress_to_dStrain_dev(dStDev, par_EL, DpI)

c         update plastic multiplier
          lamdotI = lamdot + dlamdot
          depI = lamdotI*dt
c         update the yield stress
          call Reference_Stress(par_RS, ep + depI, sy)
c         yield function
          call Equivalent_Stress(sdevI, par_ES, 1, f, grad, hess)
c         calculate residuals
          res = lamdotI*grad - DpI 

          psi = ov2*(vdot(res,res,5)*dt**2 + ((f-sy)/algmod(2,2))**2)
c         Newton-Raphson step end
c         
          if (doprint) write(*, *) 'Psi after Newton step ', psi
c
c         start line-search, if not good enough improvement
c         by the Newton-Raphson step
          j = 0  
          do while ((psi .GT. (1.d0-2.d0*beta*alpha)*psi0) 
     +             .and. (j .LE. jmax))
c           flag indicating activated line-search
            linesearch = .TRUE.
            j = j + 1
c           find new alpha as
            alpha = max(eta*alpha, (psi0*alpha**2)
     +            / (psi-(1.d0-2.d0*alpha)*psi0))
c
            if (doprint) write(*, *) 'Alpha ', alpha
c

c           update deviatoric stress
            sdevI = sdev + alpha*dsdev

c           update plastic multiplier
            lamdotI = lamdot + alpha*dlamdot
            depI = lamdotI*dt
c           update plastic strain rate
            dStDev = (strial - sdevI)/dt
            call dStress_to_dStrain_dev(dStDev, par_EL, DpI)

c           update the yield stress (Voce law)
            call Reference_Stress(par_RS, ep + depI, sy)
c           yield function
            call Equivalent_Stress(sdevI, par_ES, 1, f, grad, hess)
c           residuals
            res = lamdotI*grad -DpI
            psi = ov2*(vdot(res,res,5)*dt**2
     +          + ((f-sy)/algmod(2,2))**2)
c
            if (doprint) write(*, *) 'Psi in Linesearch ', psi
          end do
c         save max number of line-searches
          if (j .GT. jlargest) jlargest = j
c         make candidate solution being the solution of this iteration
          sdev = sdevI
          Dp = DpI

          lamdot = lamdotI
          psi0   = psi
          if (j .GT. jmax) INFO = 2
c         leave the loop if converged
          if (psi0 .LT. tol) exit
      end do
c
c     assign INFO
      if (psi .LT. tol) then
          if (linesearch) then
c             succesful return-map by use of line-search and
c             without reaching JMAX line-search iterations
              if (INFO .NE. 2) INFO = 1
          else
c             succesful return-map by pure Newton-Raphson steps
              INFO = 0
          end if
      else
c         not success
          INFO = -1
      end if
c
102   continue
c     continue from goto of the case when radial-return guess solved psi
c
c --- STRESS UPDATE (DEVIATORIC PART) -------
c
      do i=1,5
          svec(i+1) = sdev(i)
      end do
c
c --- SOLUTION-DEPENDENT VARIABLES UPDATE -------
c
c     equivalent plastic strain update
      ep = ep + lamdot*dt
      state_vars(1) = ep
      state_vars(2) = lamdot
c     isotropic hardening
CBAPTI Careful here I removed it (R)
      state_vars(3) = 0.d0
c     number of iterations used
      state_vars(4) = real(iter)
c     largest number of line-searches
      state_vars(5) = real(jlargest)
c     INFO flag
      state_vars(6) = real(INFO)
c     final value of PSI residual
      state_vars(7) = psi
c
c --- ALGORITHMIC MODULUS UPDATE -------
c     (due to major symmetry only upper triangle is calculated)
      call Equivalent_Stress(sdev, par_ES, 2, f, grad, hess)
      call Tangent_Compliance(par_EL, Cinv)
c
      do i=1, 5
        do k=i, 5
          Linv(i,k) = lamdot*hess(i,k)+ Cinv(i+1,k+1)/dt
        end do
      end do    
      call chol_decomp(Linv, 5, choldiag)
      call chol_inverse(Linv, 5, choldiag, L) 
      do i=1, 5
        y(i) = 0.d0
        do j=1, 5
          y(i) = y(i) + L(i,j)*grad(j)
        end do
      end do

      call Reference_Stress_Deriv(par_RS, ep, H)! with ep the final one
      H = H*dt
      denom = vdot(grad,y,5) + H

      call Tangent_Stiffness(par_EL, algmod)
      do j=2, 6
          algmod(1,j) = 0.d0
      end do
      do i=2, 6
          do j=i, 6
              algmod(i,j) = (L(i-1,j-1) - y(i-1)*y(j-1)/denom)/dt
          end do
      end do
c
c**********************************************************************
c**********************************************************************
101   return

      end subroutine Return_Map
