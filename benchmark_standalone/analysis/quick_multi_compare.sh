# Run a single combination of CASE and UMOCK

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

mkdir -p dat

python_args=""

for case in baushinger.F; do
 cd ./umocks/
 # There
 casename=$(basename -- "$case")
 casename="${casename%.*}"
 for umock in ZieglerPrager2_hs.F Holmedal_hs.F Isotropic_hs.F ; do
  # Here
  umatname=$(basename -- "$umock")
  umatname="${umatname%.*}"
  datname="${casename}_${umatname}"
  echo $datname
  # Now
  gfortran -cpp ../cases/${case} ${umock} 
  ./a.out > ../dat/${datname}.log
  cat ../dat/${datname}.log | grep "MAIN" | cut -c7- > ../dat/${datname}_variables.csv
  #cat ../dat/${datname}.log | grep "CONVERGENCE" | cut -c14- > ../dat/${datname}_convergence.csv"
  python_args=$python_args" ./dat/${datname}_variables.csv"
 done
 cd ..
done

python analysis/csv_plot.py 0 1 $python_args
