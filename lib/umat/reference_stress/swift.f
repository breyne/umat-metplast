C> @file swift.f Swift's hardening rule
C-----------------------------------------------------------------------
C> @author
C>    - Baptiste Reyne
C-----------------------------------------------------------------------
C> @details
C>    The hardening parameter expresses
C>    \f[
C>      \sigma_y(p) = K(\varepsilon_0+p)^n
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------
C> @param[in] params <b>(3)</b> The parameters:
C>    |Symbol               | Name              | Unit  | e.g.  |
C>    |---------------------|-------------------|-------| ------|
C>    | \f$\varepsilon_0\f$ | initial strain    | -     |  .1   |
C>    | \f$K\f$             | hardening coef    | MPa   | 250   |
C>    | \f$n\f$             | exponent          | -     |   0.5 |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] sigma_y the hardening parameter \f$\sigma_y\f$.
C
      subroutine Reference_Stress(params, aps, sigma_y)
      implicit none
      real*8      params(3), aps, sigma_y
      sigma_y = abs(params(2)*(params(1)+aps))**params(3)
      end subroutine Reference_Stress

C=======================================================================

C> @author
C>    - Baptiste Reyne

C-----------------------------------------------------------------------
C> @details
C>    The derivative yields
C>    \f[
C>      \frac{\partial\sigma_y}{\partial p}=
C>      Kn(\varepsilon_0+p)^{n-1}
C>    \f]
C-----------------------------------------------------------------------

C> @param[in] dparams <b>(3)</b> The parameters:

C>    see above

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] dsy_daps the hardening parameter \f$r\f$.
C>     
      subroutine Reference_Stress_Deriv(dparams, aps, dsy_daps)
      implicit none
      real*8      dparams(3), aps, dsy_daps
      dsy_daps =  abs(
     +  dparams(2)*dparams(3)*(dparams(1)+aps)
     +)**(dparams(3)-1.d0)
      end subroutine Reference_Stress_Deriv
