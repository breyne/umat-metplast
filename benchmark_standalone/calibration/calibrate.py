"""This scripts looks for the optimal set of parameters for a given model
to minimize its predictions compared to a reference file (experiment).
Example:
>>> bash calibration/test_generate_executables.sh
>>> python -i calibration/calibrate.py     \
>>>  -m test_hol.exe                       \ 
>>>  -r calibration/test_hardening_ref.csv \
>>>  -p calibration/test_params_hol19.csv
The output is a .fparam file
"""

import argparse
import os
import subprocess as sp
import scipy
import numpy as np
import pandas as pd
import plotly.graph_objects as go

####################################################################################################
## Deal with arguments
####################################################################################################

PARSER = argparse.ArgumentParser(
    "calibrate.py",
    description="""Optimizes parameters for a given
        combination of model, case, reference and parameter files""",
    epilog="||/||",
)
PARSER.add_argument(
    "-r",
    "--ref",
    help="Reference file, comma-separated values: strain, stress",
    type=str,
)
PARSER.add_argument("-p", "--param", help="parameters text file ", type=str)
PARSER.add_argument("-m", "--model", help="executable file ", type=str)

ARGS = PARSER.parse_args()

for arg in (
    "model",
    "ref",
    "param",
):
    if ARGS.__dict__[arg] is None:
        raise ValueError(f"Must provide option: --{arg} <FILEPATH>")
    if not os.path.isfile(ARGS.__dict__[arg]):
        raise ValueError(f"Invalid path for file: {arg}")

####################################################################################################
## Temp files
####################################################################################################

RESULTS_FILE = "/tmp/PUGCALIB_tmp_res.csv"
MODEVAL_FILE = "/tmp/PUGCALIB_tmp_modeval.bash"
FPARAMS_FILE = "/tmp/PUGCALIB_tmp_fortranparams.txt"

MODEVAL_TXT = """# Temporary bash file for easy execution
exe_file=$1
par_file=$2
res_file=$3
echo "=================="
cat ${par_file}
./${exe_file} < ${par_file} | grep MAIN | cut -c7- > $3
"""
with open(MODEVAL_FILE, 'w', encoding='UTF8') as f:
    f.write(MODEVAL_TXT)

SAVE_FILE = os.path.splitext(ARGS.param)[0]+'.fparam'

####################################################################################################
## Rerefence gathering
####################################################################################################

REF_STRAIN = pd.read_csv(ARGS.ref).values[:, 0]  #
REF_STRESS = pd.read_csv(ARGS.ref).values[:, 1]  #
PARAMS_DF = pd.read_csv(ARGS.param)

####################################################################################################
## Define funs
####################################################################################################

def write_param_file(props, filepath=None, footer=None):
    """Write a set of parameters to a file readable by fortran /stdin

    Parameters
    ----------
    props : mdarray
        One value for each row of PARAMS_DF
    filepath : str
        Where to write
    footer : str
        Something to add at the end since 
    """
    # Process options
    footstring = footer if footer else ''
    filepath = filepath if filepath else FPARAMS_FILE
    # The array has one additional zero-row
    arr = np.zeros([1+len(PARAMS_DF), 2])
    # Fill with indices and values
    arr[:-1,0] = PARAMS_DF['index']
    arr[:-1,1] = props
    # Print with correct format (integer, real*8)
    np.savetxt(
            filepath,
            arr,
            fmt=['% 4d', '% 1.6e'],
            footer=footstring,
            )

def evaluate_model():
    """Use the written file to call standalone code, and read output"""
    # Run the models evaluation (bash script in MODEVAL_TXT) with the given param file
    sp.run(['bash', MODEVAL_FILE, ARGS.model, FPARAMS_FILE, RESULTS_FILE], check=True)
    # Read the csv output
    eps_mod = pd.read_csv(RESULTS_FILE).values[:, 0]  #
    sig_mod = pd.read_csv(RESULTS_FILE).values[:, 1]  #
    return eps_mod, sig_mod

def integrated_error(eps, sig):
    """Return the stress values linearly interpolated on the reference strain axis

    Parameters
    ----------
    eps : numpy.1darray
        strain values
    sig : numpy.1darray
        stress values

    """
    # Interp on the reference axis
    sig_interp = np.interp(
            REF_STRAIN, eps, sig,
            left=np.nan, right=np.nan
            )
    # Extrapolation is forbidden, we only accept the intersection of the two axes
    valid_indices = np.where(~np.isnan(sig_interp))
    # Integrate the squared difference
    local_error = (sig_interp[valid_indices]-REF_STRESS[valid_indices])**2.
    return np.trapz(local_error, x=REF_STRAIN[valid_indices])

def merit_function(params):
    """Measures the difference between reference file and computed prediction
    """
    # Prepend params to paramfile
    write_param_file(params)
    # Evaluate model with paramfile
    eps, sig = evaluate_model()
    # Get the corresponding integral
    return integrated_error(eps, sig)

####################################################################################################
## Main matter
####################################################################################################

#write_param_file(PARAMS_DF["initial_value"].values)
#EPS, SIG = evaluate_model()

RESULT = scipy.optimize.minimize(
        merit_function,
        PARAMS_DF["initial_value"].values,
        bounds=PARAMS_DF[["min_value", "max_value"]].values,
        method='Nelder-Mead',
        )

write_param_file(RESULT.x)
EPS, SIG = evaluate_model()

write_param_file(RESULT.x, SAVE_FILE, str(PARAMS_DF))


####################################################################################################
## Figure
####################################################################################################

FIG = go.Figure()
FIG.add_trace(go.Scatter(x=REF_STRAIN, y=REF_STRESS))
FIG.add_trace(go.Scatter(x=EPS, y=SIG))
FIG.show()
