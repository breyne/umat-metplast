C This is a generalisation of tension.f to let us chose direction
C increment size and max strain


#ifndef INC_SIZE
#define INC_SIZE 1.e-3
#endif

#ifndef MAX_STRAIN
#define MAX_STRAIN 1.e0
#endif

      PROGRAM MONOLOAD
      implicit none

      ! other
      integer i, arr_idx
      real*8  d_t

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6), eps_dev(6), sig_dev(6)
      real*8  DIR_DEV(6)
      real*8  ddsdde(6,6)
      real*8 t0, t1
      integer NS, NP
      real*8 statev(200), props(200)
      real*8 eps11, eps22, eps33, eps23, eps13, eps12, EPS
      real*8 val

      call GET_SIZES_PS(NP, NS)
      call ASSIGN_PROPS(props, NP)


      stress = (/0.,0.,0.,0.,0.,0./)
      strain = (/0.,0.,0.,0.,0.,0./)
      
C READ LOADING DIRECTION ===============================================

#ifdef DEPS_FROM_STDIN
      write(*,*) "enter strain direction components 11,22,33,13,23,12"
      read (*,*) eps11, eps22, eps33, eps23, eps13, eps12 
      d_strain(1:3) = (/ eps11, eps22, eps33 /)
      d_strain(4:6) = (/ eps23, eps13, eps12 /)*(2.d0**.5)
#else
      d_strain = (/ 1., -.5, -.5, 0., 0., 0. /)
#endif
      DIR_DEV(1:3) = d_strain(1:3)-sum(d_strain(1:3))/3.d0
      DIR_DEV(4:6) = d_strain(4:6)
      DIR_DEV = DIR_DEV * sum(DIR_DEV*DIR_DEV)**(-.5)

      d_strain = d_strain * sum(d_strain*d_strain)**(-.5)
      d_strain = d_strain * INC_SIZE

C READ PROPS       =====================================================

#ifdef CALIBRATION
      arr_idx=-1
      do 
        write(*,*) "CALIBRATION ",
     +             "Input (int i>0, real x) such that props(i)=x"
        read (*,*) arr_idx, val
        write(*,*) "CALIBRATION (read)", arr_idx, val
        if (arr_idx.LT.1) exit
        props(arr_idx) = val
      end do
      write(*,*) "CALIBRATION PROPS =",props
#endif

C RUN UMAT =============================================================
      statev   = 0.d0
      d_t      = 1.d0


      write(*,*)"MAIN ",  "strain_11,stress_11,aps"
      write(*,*)"CONVERGENCE ", "inc_time,i_max,j_mean"
      
      EPS = sum(strain*DIR_DEV)

      do while (EPS.LT.MAX_STRAIN)
        CALL Umock(stress, d_strain, d_t, statev, NS, props, NP, ddsdde)
        strain = strain+d_strain
        EPS = sum(strain*DIR_DEV)
        write(*,*) "MAIN", strain(1),!sum(strain*DIR_DEV),
     +              ','  , stress(1) !sum(stress*DIR_DEV)
      enddo

      write(*,*) "MAIN props", props(:NP)
      call ASSIGN_PROPS(props, NP)
      write(*,*) "MAIN prold", props(:NP)
      END PROGRAM
       
