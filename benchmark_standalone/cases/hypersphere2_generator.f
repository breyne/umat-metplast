      program ortodist
c     find homogeneously distributed points on a hypersphere
c     of dimension up to max 5
      implicit none
      integer idim, isym,imode,imaxmax
      parameter(idim=100000)
      integer i,j,imax,idx(idim),jdx(idim),imin1,imin2,imax1,imax2
      integer k, n, ndim, ngauss,icount,icount2,icount3,nnb(idim,1000)
      integer is,itermax, nnbmax(1000),nb(100,1000),imaxcentro
      integer ngmin, ngmax, ntot
      double precision ngave,vc(5)
      double precision x(idim,5),dx(idim), dxi,s,p,stdv
      double precision dxmin,dxmax,dxave,r,xnew(5)
      double precision s11,s22,s33,s12,s23,s13,dxlevel(1000)
      isym=-1
c     
      write(*,*) "What kind of distribution?"
      write(*,*) "1=random"
      write(*,*) "2=uniform"
 202  continue
      read(*,*) imode
      if((imode.lt.1).or.(imode.gt.2))then
         write(*,*) "answer out of range, try again"
         goto 202
      endif
      if(imode.eq.1)goto 203
c     
      write(*,*) "what kind of symmetry?"
      write(*,*) "1=none, 2=centro"
 201  continue
      read(*,*) isym
      if((isym.lt.1).or.(isym.gt.2))then
         write(*,*) "answer out of range, try again"
         goto 201
      endif
 203  continue
c
      Write(*,*)"The dimesion of the hypersphere?"
      read(*,*)ndim
      if ((ndim.lt.2).or.(ndim.gt.5)) goto 203
c           
      Write(*,*)"How many points?"
      read(*,*)imax
c     
      if(imode.eq.2) then
         Write(*,*)"How many iterations?"
         read(*,*)itermax
      endif
c     
c      ndim = 5      
      ngauss=100
      if(imode.eq.1)then
         imaxmax =imax
      else
         if(isym.eq.1) imaxmax =imax
         if(isym.eq.2) imaxmax =imax*2
      endif
c     
c distribute random points on surface
c pick random Gauss distributed points in space. 
c project onto unit sphere 
      do i = 1,imax
         do j = 1,ngauss
            do n = 1,ndim
               x(i,n) = x(i,n)+2.*rand(0)-1.
            enddo
         enddo
         r=0.
         do n = 1,ndim
            r=r+x(i,n)**2
         enddo
         r=sqrt(r)
         do n = 1,ndim
            x(i,n)=x(i,n)/r
         enddo
      enddo
      if (imode.eq.1) goto 120
c     expand by adding symmetry directions
      do i =1,imax
c     centrosym: point sym through origin
         if(isym.ge.2) then
            do n = 1,ndim
               x(imax+i,n)=-x(i,n)
            enddo
         endif
      enddo
c     
      icount2=0
c
c     START ITERATION LOOP:
      do k = 1,itermax
c
c find the closest neighbour angle and vector
         dxave = 0.
         do i = 1,imax
            dx(i) = 1000000000.
            do j = 1,imaxmax
               if(j.ne.i) then
                  dxi = 0.
                  do n=1,ndim
                     dxi = dxi +x(i,n)*x(j,n)
                  enddo
                  dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                  dxi = acos(dxi)
                  dxi = abs(dxi)
                  if(dxi.lt.dx(i))then
                     dx(i) = dxi
                     idx(i) = i
                     jdx(i) = j
                  endif
               endif
            enddo
            dxave = dxave + dx(i)
         enddo
         dxave=dxave/(imax*1.)
c     
         dxmin = 1000000.
         dxmax = 0.
         do i = 1,imax
            if (dx(i).lt.dxmin) then
               dxmin = dx(i)
               imin1 = idx(i)
               imin2 = jdx(i)
            endif
            if (dx(i) .gt. dxmax) then
               dxmax = dx(i)
               imax1 = idx(i)
               imax2 = jdx(i)
            endif
         enddo
c                  
         write(*,*) 
     &   k, imin1,dxmin*180/3.1415, dxave*180/3.1415, dxmax*180/3.1415
c         
c     
c     remove the min point (imin1) and replace it by random assigned point
c
         if ((dxmin.lt.0.95*dxave).and.(icount2.le.1)) then
            do i = imin1, imax-1
               do n = 1,ndim
                  x(i,n) = x(i+1,n)
                  if(isym.ge.2) then
                     x(imax+i,n)   = x(imax+i+1,n)
                  endif
               enddo
            enddo
            imax = imax-1
            if(imax1.ge.imin1) imax1=imax1-1
            if(imax2.ge.imin1) imax2=imax2-1
c     
c add random point with good distance
c
            imax=imax+1
            icount=1
 100        continue
            icount=icount+1
            if(icount.gt.(10**ndim*imaxmax)) then
               ICOUNT2=icount2+1
               write(*,*) " GOTO 110"
               write(*,*)
               goto 110
            endif
c     pick a random Gauss distributed point in space. 
c     project onto unit sphere 
 193        continue
            i = imax
            do j = 1,ngauss
               do n = 1,ndim
                  x(i,n) = x(i,n)+2.*rand(0)-1.
               enddo
            enddo
            r=0.
            do n = 1,ndim
               r=r+x(i,n)**2
            enddo
            r=sqrt(r)
            do n = 1,ndim
               x(i,n)=-x(i,n)/r
            enddo
c     expand to ototropic selection
c     centrosym: point sym through origin
            if(isym.ge.2)then 
               do n = 1,ndim
                  x(imax+i,n)=-x(i,n)
               enddo
            endif
c     
c     check if dxi .lt. dxave?
            j=imax
            do i = 1,imaxmax
               if(i.ne.j)then
                  dxi = 0.
                  do n=1,ndim
                     dxi = dxi +x(i,n)*x(j,n)
                  enddo
                  dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                  dxi = acos(dxi)
                  dxi=abs(dxi)
                  if(dxi.lt.dxmin) goto 100
c                  if(dxi.lt.dxave) goto 100
               ENDIF
            enddo
c     
 110        continue
         endif
c
c     move points randomly a distance less than 0.5*dxave
c     ensuring that dxi>dxmin
c     
c     start loop distorting all points:
         do i = 1, imax
            icount3 = 1
 230        continue
            r=0.
            do n = 1,ndim
               xnew(n) = (rand(0)*2.-1.)*0.5*Min(dxave,1.) + x(i,n)
               r = r + xnew(n)**2
            enddo
            r=sqrt(r)
            do n = 1,ndim
               xnew(n) = xnew(n)/r
            enddo
c     check that not closer than dxmin to any other
            do j = 1,imaxmax
               if (i .ne. j) then
                  dxi = 0.
                  do n=1,ndim
                     dxi = dxi +xnew(n)*x(j,n)
                  enddo
                  dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                  dxi = acos(dxi)
                  dxi=abs(dxi)
                  if(isym.eq.3)then
c     check angle to symmetric ones of this added point
                     r=dxi
                     dxi = 0.
                     do n=1,2
                        dxi = dxi +xnew(n)**2
                     enddo
                     dxi = dxi -xnew(3)**2
                     dxi = dxi -xnew(4)**2
                     dxi = dxi +xnew(5)**2
                     dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                     dxi=min(abs(acos(dxi)),abs(acos(-dxi)))
                     r=min(dxi,r)
                     
                     dxi = 0.
                     do n=1,2
                        dxi = dxi +xnew(n)**2
                     enddo
                     dxi = dxi +xnew(3)**2
                     dxi = dxi -xnew(4)**2
                     dxi = dxi -xnew(5)**2
                     dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                     dxi=min(abs(acos(dxi)),abs(acos(-dxi)))
                     r=min(dxi,r)
                     
                     dxi = 0.
                     do n=1,2
                        dxi = dxi +xnew(n)**2
                     enddo
                     dxi = dxi -xnew(3)**2
                     dxi = dxi +xnew(4)**2
                     dxi = dxi -xnew(5)**2
                     dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                     dxi=min(abs(acos(dxi)),abs(acos(-dxi)))
                  endif 
                  dxi=min(dxi,r)
                  if(dxi.lt.dxmin) then
                     icount3 = icount3 + 1
                     if(icount3.le.10) then
c     try another pick
                        goto 230
                     else
c     give up picking, keep previous
                        goto 200
                     endif
                  endif
               endif
            enddo
c     accepted point
            do n = 1,ndim
               x(i,n) = xnew(n)
            enddo
c     expand to ototropic selection
c     centrosym: point sym through origin
            if(isym.ge.2) then
               do n = 1,ndim
                  x(imax+i,n)=-x(i,n)
               enddo
            endif
c     
 200        continue
c     end of loop of random distortions:
         enddo
c     end of loop of iterations:
      EndDo
 120  continue
c find the closest neighbour angle and vector
         dxave = 0.
         do i = 1,imax
            dx(i) = 1000000000.
            do j = 1,imaxmax
               if(j.ne.i) then
                  dxi = 0.
                  do n=1,ndim
                     dxi = dxi +x(i,n)*x(j,n)
                  enddo
                  dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
                  dxi = acos(dxi)
                  dxi = abs(dxi)
                  if(dxi.lt.dx(i))then
                     dx(i) = dxi
                     idx(i) = i
                     jdx(i) = j
                  endif
               endif
            enddo
            dxave = dxave + dx(i)
         enddo
         dxave=dxave/(imax*1.)
c     
         dxmin = 1000000.
         dxmax = 0.
         do i = 1,imax
            if (dx(i).lt.dxmin) then
               dxmin = dx(i)
               imin1 = idx(i)
               imin2 = jdx(i)
            endif
            if (dx(i) .gt. dxmax) then
               dxmax = dx(i)
               imax1 = idx(i)
               imax2 = jdx(i)
            endif
         enddo
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     print results
c     
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc     
      open(11,file="hypersphere2_close_ngb_distr.dat")
      stdv = 0.
      do i = 1,imax
         stdv=stdv+(dx(i)-dxave)**2
      enddo
      stdv=sqrt(stdv/(1.*imax))
c     Kernel Density Estimator, nearest neighbour distribution
c     Scotts rule
      stdv=stdv*(1.*imax)**(-0.2)
      do is=0,10000
         s=is*1.2*dxmax/10000.
         p = 0.
         do i =1,imax
            p = p + exp(-0.5*((s-dx(i))/stdv)**2)/sqrt(6.28)
         enddo
         p=p/(1.*imax)
         write(11,*)s*180/3.1415,p
      enddo
      close(11)

      open(13,file="hypersphere2_points.dat")
      do i = 1,imax
         if(ndim.eq.2) then
            write(13,*) x(i,1),x(i,2)
         endif
         if(ndim.eq.3) then
            write(13,*) x(i,1),x(i,2),x(i,3)
         endif
         if(ndim.eq.4) then
            write(13,*) x(i,1),x(i,2),x(i,3),x(i,4)
         endif
         if(ndim.eq.5) then
            write(13,*) x(i,1),x(i,2),x(i,3),x(i,4),x(i,5)
         endif
      enddo
      close(13)
c     
c     
c     the number of points within dxlevel distribution
c     create a list of deviations to be plotted
      do is = 1,1000
         dxlevel(is)= dxave + 0.7*dxave*(is-1.)/999.
         nnbmax(is)=0
      enddo
      do i = 1,imaxmax
         do is=1,1000
            nnb(i,is) = 0
         enddo
         do j = 1,imaxmax
            if (i .ne. j) then
               dxi = 0.
               do n=1,ndim
                  dxi = dxi +x(i,n)*x(j,n)
               enddo
               dxi=min(0.9999999999999,max(-0.99999999999999,dxi))
               dxi = acos(dxi)
               dxi=abs(dxi)
               do is=1,1000
                  if(dxi.lt.dxlevel(is)) then
                     nnb(i,is) = nnb(i,is)+1
                     nnbmax(is)=max(nnbmax(is),nnb(i,is))
                  endif
               enddo
            endif
         enddo
      enddo

      open(13,file="hypersphere2_nb_ngb_distr.dat")
      do is=1,1000
         if(nnbmax(is).ge.1)then
            do i = 1,nnbmax(is)
               nb(i,is)=0
            enddo
c     nb(j,is) counts the number points with j neighbours within dxlevel(is)
            do i = 1,imaxmax
               nb(nnb(i,is),is) = nb(nnb(i,is),is) + 1
            enddo
            ngmin=1000000000
            ngmax=-1
            ngave=0.
            ntot=0
            do j=1,nnbmax(is)
               if(nb(j,is).gt.0) then
                  ngmax=max(j,ngmax)
                  ngmin=min(j,ngmin)
                  ngave=ngave+nb(j,is)*j
                  ntot=ntot+nb(j,is)
               endif
            enddo
            ngave=ngave/(1.*ntot)
            write(13,*) dxlevel(is)*180./3.1415,ngave,ngmin,ngmax
         endif
      enddo
      close(13)

      end
