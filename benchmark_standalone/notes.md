# USE-CASES

This is a set of tools and applications for PUG.

**WARNING** They rely on a combination of: `python`, `bash`, `gfortran` with (cpp) preprocessing options.

The structure is as follows.

- `./cases/` mechanical load definitions.
- `./umocks/` contains pug-generated *umock* files with CPP modifications.
- `./dat/` (gitignored)        contains printed out results .
- `./calibration/` implements parameter optimization tools

NB. the fortran files extension is here `.F` (capital) because of the use of preprocessing options.

The idea is to run `gfortran -cpp <casename> <umockname>`
Then use preprocessing options to do some cool stuff.

## Analysis

**cases** must printout a scalar strain, stress at each increment

```fortran
write(*,*) "MAIN", strain(1), ',', stress(1)
```

```fortran
#ifndef INC_SIZE
#define INC_SIZE 5.e-3
#endif

#ifndef MAX_STRAIN
#define MAX_STRAIN 1.e0
#endif
```


**umocks** must print out

```fortran
CALL CPU_TIME(T_END)
write(*,*) "CONVERGENCE", t_end-t_start,",", i,",", ls_sum/(i+1)
```

before `RETURN`; that is

- the step CPU time
- the total number of newton iterations
- the average number of LS iterations

Also expected to be CPP-friendly with the 3 following CPP variables

```
c=======================================================================
c These lines are for the c preprocessor
c     >>> gfortran -cpp -DLSMAX=20
c ----------------------------------------------------------------------
c RES_SIZE is the target size of the residual (psi_tol)
#ifndef RES_SIZE
#define RES_SIZE 1.e-20
#endif
c ----------------------------------------------------------------------
c BEST_FIRST_GUESS is to optimize the newton initial stress guess
#ifndef BEST_FIRST_GUESS
#define BEST_FIRST_GUESS
#endif
c ----------------------------------------------------------------------
c LS_MAX is the maximum number 
#ifndef LS_MAX
#define LS_MAX 0
#endif
c=======================================================================
```
and their in-code implementation.

This way we can for example write
```sh
gfortran -cpp -DLS_MAX=20 -UBEST_FIRST_GUESS case/file.F umock/other.F
```
To compile a standalone with line search and without initial guess.

Example: the files `analysis/increment_and_residual_*` use this to compare all the combinations.

## Calibration
Modify the `case` file so that props can be read from input:

```
! READ PROPS       =====================================================
#ifdef CALIBRATION
      i=-1
      do 
        write(*,*) "CALIBRATION ",
     +             "Input (int i>0, real x) such that props(i)=x"
        read (*,*) i, val
        write(*,*) "CALIBRATION (read)", i, val
        if (i.LT.1) exit
        props(i) = val
      end do
      write(*,*) "CALIBRATION PROPS =",props
#endif
```

then it can be executed feeding a parameter files like so:

```sh
gfortran -cpp -DCALIBRATION case/file.F umock/other.F
./a.out < params.txt
```

With `params.txt` a file containing rows of index, value.
This will be used as interface for calibration.

Use script `calibrate.py` to run a python optimization like so:

```bash
python calibration/calibrate.py             \
    -m <EXECUTABLE_FILE>                          \
    -r <REFERENCE_CSV>   \
    -p <PARAM_OPTIMIZATION_CSV>
```

Here 

- `<EXECUTABLE_FILE>` is the one compiled with `-DCALIBRATION` as previously shown
- `<REFERENCE_CSV>` the target curve with strain, stress in columns 1 and 2.

```txt
STRAIN,STRESS
  4.0824828133880263E-003 ,   18.435301368691668
  8.1649656267760527E-003 ,   21.183817182572881
           ...            ,         ...         
 0.99612580646668425      ,   134.30306769355761
  1.0002082892800723      ,   134.31085620109064
```
- `<PARAM_OPTIMIZATION_CSV>` following the following format to perform the optimization.

```txt
index,initial_value,min_value,max_value,name
   3 ,   24.       , 20      , 25      , Voce
   4 ,   150       , 100     , 400     , Voce
   5 ,   .4        , 0.01    , 0.5     , Voce
  25 ,   2.5       , 1.e-2   , 20      , Ziegler strain scale
```

If all goes well the resulting parameters are written in a `<>.fparam` file.

## Guess scale
Use `gfortran -c -cpp -DBEST_FIRST_GUESS -DGUESS_SCALE=0.5`

## Comments

**analysis** contains theme-based sets of scripts

- `test_single_combo.sh` and  `gen_all_combinations.sh` try some umocks with cases together
- `csv_plot.py` plots the generated csv_data
- `increment_and_residual_*` generate convergence data, pgfplots friendly
