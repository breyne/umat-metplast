#pylint: disable=undefined-all-variable
"""The client module defines tools to build and interact with
different Abq/Umat components.
"""



# Special variables
__all__ = [
        "utils_devfile",
        "utils_inp",
        "utils_userfile",
        "paths",
        "parser",
        ]

import os
with open(
        os.path.join(os.path.dirname(__file__),'../../LICENCE.md'),
        'r'
        ) as lic_file:
    __licence__ = lic_file.read()


__author__ = 'Baptiste Reyne'
__autor_website__ = 'orcid.org/0000-0003-2310-864X'

DEFAULT_NAME = 'PUG'

# Logging features
import logging # pylint: disable=wrong-import-position
logging.basicConfig(
#        filename='EXAMPLE.log',
#        filemode='w',
#        format='[%(asctime)s] %(levelname)s %(message)s (%(filename)s line %(lineno)d)',
        format=              '\033[1;%dm %(levelname)s %(message)s (%(filename)s line %(lineno)d)',
        datefmt='%y/%m/%d_%H:%M:%S',
        )
