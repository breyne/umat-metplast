

      PROGRAM UMOCK_TESTER
      implicit none

      ! other
      integer n_inc, i
      real*8  d_t

      ! Tensorial directions
      real*8  dir_tension_1(6), dir_shear_1(6),
     +        dir_tension_2(6), dir_shear_2(6),
     +        dir_pressure(6)

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6), statev(6), props(25)
      real*8  ddsdde(6,6)

      stress = (/0.,0.,0.,0.,0.,0./)
      statev = (/ 0.,
     +            0.,0.,0.,0.,0.
     +          /)

      props=(/70000.0, 0.33, 20., 150., 0.5, 4.0, 0.0, 0.0,
     +            1.0, 0.00, 0.0, 1.0 , 1.0, 1.0, 1.0, 0.0,
     +            0.0, 1.00, 0.0, 0.0 , 1.0, 1.0, 1.0, 1.0,
     +            1./)
      props=(/
     +        7.e+04, 0.3  , 20. ,150., 0.5, 8.,
     +        0.0   , 0.0   , 0.7305,
     +        0.1342,0.1342 , 0.7355, 0.922,  0.637 , 0.901,
     +        0.0   , 0.0   , 0.7305, 0.1342, 0.1342, 0.7355,
     +        0.922 , 0.637 , 0.901, 
     +        1.0
     +       /)
      props=(/
     +  7.e+04, 0.3  ,                 ! (1-2)elast 
     +  20.   ,150., 0.5,             ! (3-7)Voce eqn
     +  8.,                           ! (8) a
     +  0.0 , 0.0, 0.73, 0.134, 0.134, 0.736, 0.922, 0.637, 0.901,
     +  0.0 , 0.0, 0.73, 0.134, 0.134, 0.736, 0.922, 0.637, 0.901, 
     +  1.0
     +  /)
      d_strain = (/ 1., 1., 2., 0., 0., 0. /) * 0.001
      d_t = 1.d0

      n_inc = 5
      do i=1,n_inc
        write(*,*) "..."
        write(*,*) "==================================================="
        CALL Umock(stress, d_strain, d_t, statev, 6, props, 25, ddsdde)
        write(*,*) "final acp:", statev(1)
        write(*,*) "final X:", statev(2:6)
        write(*,*) "final stress:", stress
        write(*,*) "final ddsdde:", ddsdde(1,:)
        write(*,*) "            :", ddsdde(2,:)
        write(*,*) "            :", ddsdde(3,:)
        write(*,*) "            :", ddsdde(4,:)
        write(*,*) "            :", ddsdde(5,:)
        write(*,*) "            :", ddsdde(6,:)
        write(*,*) "---------------------------------------------------"
      enddo

      END PROGRAM UMOCK_TESTER
              
      include "pool/PUG_zp_tester.f"
