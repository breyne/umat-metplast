""" This scripts plot out the result of the bash generator
"""
import pandas as pd
import numpy as np
import plotly.graph_objects as go


CASE_GS='g1s0'
CASE_UN='ten_UHolm'

FILE_STR='dat/irsizes/'+CASE_GS+'/'+CASE_UN+'_I{INC:.0e}_R{RES:.0e}'#.format(3e-4, 1e-12)

R_SIZES = pd.read_csv('analysis/increment_and_residual_PSITOLS.txt').values[:,0]
I_SIZES = pd.read_csv('analysis/increment_and_residual_ISIZES.txt').values[:,0]
R_SIZES.sort()
I_SIZES.sort()
#R_SIZES=np.flip(R_SIZES)
#I_SIZES=np.flip(I_SIZES)


T_MEAN_IR = np.zeros([len(I_SIZES), len(R_SIZES)])
I_MEAN_IR = np.zeros([len(I_SIZES), len(R_SIZES)])
J_MEAN_IR = np.zeros([len(I_SIZES), len(R_SIZES)])
ERR_IR = np.zeros([len(I_SIZES), len(R_SIZES)])

ERR_REF_SERIE = None
NORMALIZE = None

BOOL_WH = True
if BOOL_WH:
    FIG = go.Figure()

# Loop through files and read all
for row, inc_s in enumerate(I_SIZES):
    for col, res_s in enumerate(R_SIZES):
        print(row, col)
        FILENAME = FILE_STR.format(INC=inc_s, RES=res_s)
        print(FILENAME)
        data = pd.read_csv(FILENAME+'_convergence.csv').values
        T_MEAN_IR[row,col] = np.average(data[:,0])
        I_MEAN_IR[row,col] = np.average(data[:,1])
        J_MEAN_IR[row,col] = np.average(data[:,2])
        # For the error we'll give something if we actually have a result
        data = pd.read_csv(FILENAME+'_variables.csv').values
        #data[:,1] = np.abs(data[:,1])
        if row == col == 0:#:the best is for minimum incsize and minimum ressize
            ERR_REF_SERIE = data[:,0:3]
            NORMALIZE = np.trapz(data[:,1], x=data[:,2])
            ERR_IR[row,col] = 0.
        elif data.dtype == np.dtype('O'):
            ERR_IR[row,col] = np.nan
        else:
            sig_interp = np.interp(data[:,2], ERR_REF_SERIE[:,2],  ERR_REF_SERIE[:,1])
            ERR_IR[row,col] = np.trapz(
                    np.abs(sig_interp-data[:,1]),
                    x=data[:,2])
        if BOOL_WH:
            FIG.add_trace(go.Scatter(
                x=data[:,0], y=data[:,1],name=str(inc_s)+str(res_s),#mode='markers',#))
                ))


if BOOL_WH:
    FIG.show()

ERR_IR = abs(ERR_IR/NORMALIZE)

#ERR_IR = abs(ERR_IR/ERR_IR[0,0]-1)
ERR_IR[ERR_IR>2.] = 2.
ERR_IR[np.isnan(ERR_IR)] = 2.

###############################################################################################
###############################################################################################

# CONVERGENCE FIGURES
# --------------------

#FIG = go.Figure()
#for col, res_s in enumerate(R_SIZES):
#    FIG.add_trace(go.Scatter(
#        x=np.log(I_SIZES), y=ERR_IR[:,col],name=str(res_s),#mode='markers',#
#        ))
#FIG.show()

# ERROR FIGURES
# --------------------

FIG = go.Figure(
        data=[
            go.Surface(x=np.log(I_SIZES), y=np.log(R_SIZES), z=np.log(ERR_IR)),
            #go.Surface(x=np.log(I_SIZES), y=np.log(R_SIZES), z=np.log(T_MEAN_IR)),
            #go.Surface(x=np.log(I_SIZES), y=np.log(R_SIZES), z=np.log(I_MEAN_IR)),
            #go.Surface(x=np.log(I_SIZES), y=np.log(R_SIZES), z=np.log(J_MEAN_IR)),
              ])
FIG.show()

###############################################################################################
###############################################################################################


PGF_TEXT='ressize\tincsize\tmean_time\tmean_nNewt\tmean_nLS\trel_error\n'
for row, inc_s in enumerate(I_SIZES):
    PGF_TEXT+='\n'
    for colinv, res_s in enumerate(np.flip(R_SIZES)):
        col = len(R_SIZES)-1-colinv
        PGF_TEXT+=('{:.3e}\t'*6+'\n').format(
                res_s, inc_s,
                T_MEAN_IR[row,col],
                I_MEAN_IR[row,col],
                J_MEAN_IR[row,col],
                ERR_IR[row,col],
                )
        #ERR_TEXT+='{:.3e}\t{:.3e}\t{:.3e}\n'.format(row, col, ERR_IR[row,col])
        #CON_TEXT+='{:.3e}\t{:.3e}\t{:.3e}\n'.format(row, col, TMAX_IR[row,col])

with open("dat/PGF_DATA/irsizes_"+CASE_UN+'_'+CASE_GS+".txt", "w", encoding='utf8') as text_file:
    text_file.write(PGF_TEXT)
