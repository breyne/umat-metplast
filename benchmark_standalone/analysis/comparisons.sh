# Run tension with different step sizes, then compare

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

mkdir -p dat
mkdir -p dat/compare
mkdir -p dat/PGF_DATA

csv_args=""

#for casename in crossload.F; do
for casename in baushinger.F crossload.F; do
 gfortran -cpp -DINC_SIZE=5.E-3 cases/${casename} -c -o tmp_case.o
 for umockname in Holmedal_hs.F Isotropic_hs.F ZieglerPrager2_hs.F; do
  basename="${casename:0:3}_${umockname:0:3}"
  basename="${basename,,}"
  if [ -f "./dat/compare/g0s0_${basename}.log" ]; then
   echo "(EXISTS) $basename"
  else
   echo $basename
   gfortran -cpp                    -DLS_MAX=0  -DRES_SIZE=1.E-20 tmp_case.o umocks/$umockname #G0S0
   ./a.out > ./dat/compare/g0s0_${basename}.log
   gfortran -cpp -DBEST_FIRST_GUESS -DLS_MAX=0  -DRES_SIZE=1.E-20 tmp_case.o umocks/$umockname #G1S0
   ./a.out > ./dat/compare/g1s0_${basename}.log
   gfortran -cpp                    -DLS_MAX=50 -DRES_SIZE=1.E-20 tmp_case.o umocks/$umockname #G0S1
   ./a.out > ./dat/compare/g0s1_${basename}.log
   gfortran -cpp -DBEST_FIRST_GUESS -DLS_MAX=50 -DRES_SIZE=1.E-20 tmp_case.o umocks/$umockname #G1S1
   ./a.out > ./dat/compare/g1s1_${basename}.log
  fi
  csv_args=$csv_args" ./dat/compare/g1s1_${basename}_variables.csv"
 done
done

for file in dat/compare/*.log; do
 cat $file | grep "MAIN"        | cut -c7-  > ${file%%.*}_variables.csv
 cat $file | grep "CONVERGENCE" | cut -c14- > ${file%%.*}_convergence.csv
done


python analysis/csv_plot.py 0 1 $csv_args
