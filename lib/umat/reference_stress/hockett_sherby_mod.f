
C> @file hockett_sherby_mod.f HS + linear term
C-----------------------------------------------------------------------
C> @author
C>    - Baptiste Reyne
C-----------------------------------------------------------------------
C> @details
C>    The yield stress is here
C>    \f[
C>      \sigma_y(p) = R_\mathrm{sat} - (R_\mathrm{sat}-R_0)
C>       \exp\left[-\left(\frac{p}{p_\mathrm{sat}}\right)^n\right]
C>       + K p
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------
C> @param[in] params <b>(5)</b> The parameters:
C>    |Symbol                 | Name              | Unit  | e.g.  |
C>    |-----------------------|-------------------|-------| ------|
C>    | \f$R_\mathrm{sat}\f$  | saturation stress | MPa   | 600.0 |
C>    | \f$R_0\f$             | initial stress    | MPa   | 150.0 |
C>    | \f$p_\mathrm{sat}\f$  | saturation strain | -     |   0.5 |
C>    | \f$K\f$               | strength coef     | MPa   | 355.5 |
C>    | \f$n\f$               | strength exponent | -     |   0.5 |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] sigma_y the hardening parameter \f$\sigma_y\f$.
C
      subroutine Reference_Stress(params, aps, sigma_y)
      implicit none

      !i/o
      real*8      params(5), aps, sigma_y
      !internal
      real*8      R0, RSAT, PSAT, N, K

      RSAT = params(1)
      R0   = params(2)
      Psat = params(3)
      K    = params(4)
      N    = params(5)

      sigma_y = K*aps + RSAT
      sigma_y = sigma_y - (RSAT-R0)*exp(-abs(aps)**N/PSAT)
      !write(6,*) "sigy=", sigma_y
      end subroutine Reference_Stress


C=======================================================================

C> @author
C>    - Baptiste Reyne: refactoring for Doxygen and PUG

C-----------------------------------------------------------------------
C> @details
C>    The hardening parameter yields
C>    \f[
C>      \frac{\partial\sigma_y}{\partial p}= K+
C>      n\frac{R_\mathrm{sat}-R_0}{p_\mathrm{sat}}
C>      \left(\frac{p}{p_\mathrm{sat}}\right)^{n-1}
C>       \exp\left[-\left(\frac{p}{p_\mathrm{sat}}\right)^{n}\right]
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------

C> @param[in] dparams <b>(5)</b>  The parameters:

C>    |Symbol                | Name              | Unit  |
C>    |----------------------|-------------------|-------|
C>    | \f$R_\mathrm{sat}\f$ | saturation stress | MPa   |
C>    | \f$R_0\f$            | initial stress    | MPa   |
C>    | \f$p_\mathrm{sat}\f$ | saturation strain | -     |
C>    | \f$K\f$              | strength coef     | MPa   |
C>    | \f$n\f$              | strength exponent | -     |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] dsy_daps the hardening parameter \f$r\f$.
C>     
      subroutine Reference_Stress_Deriv(dparams, aps, dsy_daps)
      implicit none

      !i/o
      real*8      dparams(5), aps, dsy_daps, tmp
      !internal
      real*8      R0, RSAT, PSAT, N, K

      RSAT = dparams(1)
      R0   = dparams(2)
      Psat = dparams(3)
      K    = dparams(4)
      N    = dparams(5)


      !Apparently negative exponents are handled weirdly by the compiler
      !Also singular in 0 for exponents N<1

      APS = max(APS, 1.d-42)

      IF (N.GE.1.D0) THEN
        dsy_daps = exp(-(aps)**N/PSAT) * N*(aps**abs(N-1.))/PSAT
      ELSE
        dsy_daps = exp(-(aps)**N/PSAT) * N/(aps**abs(N-1.))/PSAT
      ENDIF
      dsy_daps = dsy_daps*(RSAT-R0) + K

      !call Reference_Stress(dparams, aps,dsy_daps)
      !call Reference_Stress(dparams, aps+1.d-6,tmp)
      !dsy_daps = (tmp-dsy_daps)/1.d-6

      end subroutine Reference_Stress_Deriv
