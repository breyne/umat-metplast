#ifndef INC_SIZE
#define INC_SIZE 1.e-2
#endif


      PROGRAM MULTIDIR
      implicit none

      integer LINE
      real*8  d_t

      ! internal variables
      real*8 FD(6)
      real*8  stress(6), strain(6), d_strain(6)
      real*8  ddsdde(6,6)

      real*8 t0, t1

      integer NS, NP
      real*8 statev(200), props(200), val
      call GET_SIZES_PS(NP, NS)
      call ASSIGN_PROPS(props, NP)


      stress = (/0.,0.,0.,0.,0.,0./)
      strain = (/0.,0.,0.,0.,0.,0./)
      
      write(*,*)"MAIN ",  "strain_11,stress_11,aps"
      write(*,*)"CONVERGENCE ", "inc_time,i_max,j_mean"

      open (101, file = '10000_5D.dat')
      do LINE = 1,10000
        read(101,*) FD
        !write(*,*) LINE, FD(6)
        d_strain(2) = (+(3+sqrt(3.d0))*FD(1)-(3-sqrt(3.d0))*FD(2))/6
        d_strain(3) = (-(3-sqrt(3.d0))*FD(1)+(3+sqrt(3.d0))*FD(2))/6
        d_strain(1) = -d_strain(2)-d_strain(3)
        d_strain(4) = FD(3)/sqrt(2.d0)
        d_strain(5) = FD(4)/sqrt(2.d0)
        d_strain(6) = FD(5)/sqrt(2.d0)  
        d_strain = d_strain * sum(d_strain*d_strain)**(-.5) * INC_SIZE  
        statev=0.
        stress=0.
        CALL Umock(stress, d_strain, d_t, statev, NS, props, NP, ddsdde)
        strain = strain+d_strain
        write(*,*) "MAIN", strain(1), ',', stress(1), ',', statev(1)
      end do 
      close(101)

      END PROGRAM MULTIDIR
