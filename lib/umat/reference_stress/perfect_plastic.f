C> @file perfect_plastic.f No hardening.
C-----------------------------------------------------------------------
C> @author Baptiste Reyne
C-----------------------------------------------------------------------
C> @brief A unique yield value.
C> 
C> @details
C>    The hardening parameter yields
C>    \f[
C>      \sigma_y(p) = \sigma_0 
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------
C> @param[in] params <b>(1)</b> The parameter:

C>    |Symbol                | Name              | Unit  |
C>    |----------------------|-------------------|-------|
C>    | \f$\sigma_0\f$       | yield stress      | MPa   |

C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] sigma_y the hardening parameter \f$\sigma_y\f$.
C
      subroutine Reference_Stress(params, aps, sigma_y)
      implicit none
      real*8      params(1), aps, sigma_y
      sigma_y = params(1)
      !write(6,*) "sigy=", sigma_y
      end subroutine Reference_Stress

C=======================================================================

C> @author Baptiste Reyne
C-----------------------------------------------------------------------
C> @details
C>    The hardening parameter yields
C>    \f[
C>      \frac{\partial\sigma_y}{\partial p}=0
C>    \f]
C>    where \f$p\f$ is the accumulated plastic strain.
C-----------------------------------------------------------------------
C> @param[in] dparams <b>(1)</b> The parameter (see above).
C> @param[in] aps the accumulated plastic strain \f$p\f$.
C> @param[out] dsy_daps the hardening parameter \f$r\f$.
C>     
      subroutine Reference_Stress_Deriv(dparams, aps, dsy_daps)
      implicit none
      real*8      dparams(1), aps, dsy_daps
      dsy_daps =  0.
      end subroutine Reference_Stress_Deriv
