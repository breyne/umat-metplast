C> @file voigt_to_natural.f Conversion from Voigt to natural basis.
C>

C> @brief Solution processing subroutine at the start of the return
C>    mapping algorithm
C> @param[in] STRESS as provided by Abaqus (Voigt)
C> @param[out] svec natural basis stress
C> @param[in] DSTRAN as provided by Abaqus (Voigt)
C> @param[out] dstrain natural notation strain increment
C>

      subroutine voigt_to_natural(STRESS, svec, DSTRAN, dstrain)

      implicit none 
      real*8 ovsqrt3, ovsqrt2, ovsqrt6, sqrt2
      real*8 STRESS(6),svec(6), dstrain(6), DSTRAN(6)

      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt2 = 1.d0/ovsqrt2

      svec(1) = ovsqrt3*(STRESS(1)+STRESS(2)+STRESS(3))
      svec(2) = ovsqrt6*(2.d0*STRESS(3)-STRESS(1)-STRESS(2))
      svec(3) = ovsqrt2*(STRESS(2)-STRESS(1))
      svec(4) = sqrt2*STRESS(6)
      svec(5) = sqrt2*STRESS(5)
      svec(6) = sqrt2*STRESS(4)
      
      dstrain(1) = ovsqrt3*(DSTRAN(1)+DSTRAN(2)+DSTRAN(3))
      dstrain(2) = ovsqrt6*(2.d0*DSTRAN(3)-DSTRAN(1)-DSTRAN(2))
      dstrain(3) = ovsqrt2*(DSTRAN(2)-DSTRAN(1))
      dstrain(4) = ovsqrt2*DSTRAN(6)
      dstrain(5) = ovsqrt2*DSTRAN(5)
      dstrain(6) = ovsqrt2*DSTRAN(4)

      end subroutine  voigt_to_natural

C> @brief Solution processing subroutine at the end of the return
C>    mapping algorithm.

C> @param[out] STRESS to return to Abaqus (Voigt)
C> @param[in] svec Stress in natural basis
C> @param[in] am Algorithmic modulus in natural basis.
C> @param[out] DDSDDE as needed by  Abaqus (Voigt)
C>

      subroutine natural_to_voigt(STRESS, svec, am, DDSDDE)
      implicit none 
      real*8 ovsqrt3, ovsqrt2, ovsqrt6, sqrt3, ov2, ov3
      real*8 STRESS(6),svec(6)
      real*8 DDSDDE(6,6), am(6,6)
      integer i, j

      ov2 = 0.5d0
      ov3 = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt3 = 1.d0/ovsqrt3
c   
      STRESS(1) = ovsqrt3*svec(1) - ovsqrt6*svec(2) - ovsqrt2*svec(3)
      STRESS(2) = ovsqrt3*svec(1) - ovsqrt6*svec(2) + ovsqrt2*svec(3)
      STRESS(3) = ovsqrt3*svec(1) + 2.d0*ovsqrt6*svec(2)
      STRESS(4) = ovsqrt2*svec(6)
      STRESS(5) = ovsqrt2*svec(5)
      STRESS(6) = ovsqrt2*svec(4)
c
c ---- ALG. MODULUS CONVERSION FROM NATURAL TO ABQ-VOIGT NOTATION -----
c 
      DDSDDE(1,1) = ov3*(am(1,1) + ov2*am(2,2) + sqrt3*am(2,3)
     +            + 3.d0/2.d0*am(3,3))
      DDSDDE(1,2) = ov3*(am(1,1) + ov2*am(2,2) - 3.d0/2.d0*am(3,3))
      DDSDDE(1,3) = ov3*(am(1,1) - am(2,2) - sqrt3*am(2,3))
      DDSDDE(1,4) = -ov2*(ovsqrt3*am(2,6) + am(3,6))
      DDSDDE(1,5) = -ov2*(ovsqrt3*am(2,5) + am(3,5))
      DDSDDE(1,6) = -ov2*(ovsqrt3*am(2,4) + am(3,4))
      DDSDDE(2,2) = ov3*(am(1,1) + ov2*am(2,2) - sqrt3*am(2,3)
     +            + 3.d0/2.d0*am(3,3))
      DDSDDE(2,3) = ov3*(am(1,1) - am(2,2) + sqrt3*am(2,3))
      DDSDDE(2,4) = -ov2*(ovsqrt3*am(2,6) - am(3,6))
      DDSDDE(2,5) = -ov2*(ovsqrt3*am(2,5) - am(3,5))
      DDSDDE(2,6) = -ov2*(ovsqrt3*am(2,4) - am(3,4))
      DDSDDE(3,3) = ov3*(am(1,1) + 2.d0*am(2,2))
      DDSDDE(3,4) = ovsqrt3*am(2,6)
      DDSDDE(3,5) = ovsqrt3*am(2,5)
      DDSDDE(3,6) = ovsqrt3*am(2,4)
      DDSDDE(4,4) = ov2*am(6,6)
      DDSDDE(4,5) = ov2*am(5,6)
      DDSDDE(4,6) = ov2*am(4,6)
      DDSDDE(5,5) = ov2*am(5,5)
      DDSDDE(5,6) = ov2*am(4,5)
      DDSDDE(6,6) = ov2*am(4,4)
c     and symmetric lower triangle
      do i=1, 6
          do j=i+1, 6
              DDSDDE(j,i) = DDSDDE(i,j)
          end do
      end do

      RETURN

      end subroutine natural_to_voigt
