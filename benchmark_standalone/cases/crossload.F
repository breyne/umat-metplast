#ifndef INC_SIZE
#define INC_SIZE 1.e-3
#endif

      PROGRAM BAUSH
      implicit none

      ! other
      integer n_inc, i
      real*8  d_t

      ! internal variables
      real*8  stress(6), strain(6), d_strain(6)
      real*8  ddsdde(6,6)

      real*8 t0, t1, E_CUMUL

      integer NS, NP
      real*8 statev(200), props(200)
      call GET_SIZES_PS(NP, NS)
      call ASSIGN_PROPS(props, NP)


      stress = (/0.,0.,0.,0.,0.,0./)
      strain = (/0.,0.,0.,0.,0.,0./)

      statev = 0.d0
      d_t      = 1.d0
      n_inc    = 333

      E_CUMUL = 0.d0

      write(*,*)"MAIN ",  "strain,stress"
      write(*,*)"CONVERGENCE ", "inc_time,i_max,j_mean"


      d_strain = (/ 1., -.5, -0.5, 0., 0., 0. /)
      d_strain = d_strain * sum(d_strain*d_strain)**(-.5) * INC_SIZE
      do i=1,n_inc
        CALL Umock(stress, d_strain, d_t, statev, NS, props, NP, ddsdde)
        strain = strain+d_strain
        E_CUMUL = E_CUMUL + d_strain(1)
        write(*,*) "MAIN", E_CUMUL, ',', stress(1)
      enddo

      d_strain = (/ -0.5, +1.0, -0.5, 0., 0., 0. /)
      d_strain = d_strain * sum(d_strain*d_strain)**(-.5) * INC_SIZE
      do i=1,2*n_inc
        CALL Umock(stress, d_strain, d_t, statev, NS, props, NP, ddsdde)
        strain = strain+d_strain
        E_CUMUL = E_CUMUL + d_strain(2)
        write(*,*) "MAIN", E_CUMUL, ',', stress(2)
      enddo




      END PROGRAM BAUSH
       
