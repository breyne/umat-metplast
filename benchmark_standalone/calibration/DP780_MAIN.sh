#!/bin/sh

# Move to benchmark_standalone
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..

### # Compile executables
gfortran -cpp -DMAX_STRAIN=.2               -DINC_SIZE=3.e-3 -DLS_MAX=100 -DBEST_FIRST_GUESS cases/tension.F umocks/Isotropic_hs.F -o DP780_iso.exe
gfortran -cpp -DMAX_STRAIN=.2 -DCALIBRATION -DINC_SIZE=2.e-3 -DLS_MAX=100 -DBEST_FIRST_GUESS cases/tension.F umocks/ZieglerPrager2_hs.F -o DP780_zie.exe
gfortran -cpp -DMAX_STRAIN=.2 -DCALIBRATION -DINC_SIZE=2.e-3 -DLS_MAX=100 -DBEST_FIRST_GUESS cases/tension.F umocks/Holmedal_hs.F  -o DP780_hol.exe
 
# Produce reference file
./DP780_iso.exe | grep MAIN | cut -c7- > calibration/DP780_hardening_ref.csv


# Then calibrate with 
python   calibration/calibrate.py             \
      -m DP780_zie.exe                        \
      -r calibration/DP780_hardening_ref.csv  \
      -p calibration/DP780_params_zie.csv

