""" This scripts plot out the result of the bash generator
In order to make the results more readable and comparable,
some cloud of points are reduced in size.
"""
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import itertools
import plotly.graph_objects as go

NEW_LEN = 500
HEADERS = {
    "CONV": "inc_time\tavg_Newt_iters\tavg_LS_iters",
    "HARD": "strain\tstress",
}

CASE="mul"
UMOCK_LIST=("iso","hol",)#
UMOCK_LIST=("iso","zie","hol",)#

SCALE_LIST = np.arange(.1, 2.1, .1)

SUMMARY = np.zeros([100,len(SCALE_LIST)])


def coarsen_aray(arr, new_len):
    """Average packs of value in given array
    Arguments
    ---------
    arr:
        numpy array of shape (N,M) to coarsen
    new len : int
        length of the new averaged array (<N)

    Returns
    -------
    new_array: ndarray
        numpy array of shape (new_len,M) after coarsening
    """
    reste = len(arr) % new_len
    new_arr = np.zeros(arr.shape)[:new_len]
    grp_size = int(len(arr) / new_len)
    row_start = 0
    row_end = 0
    for i_grp in range(new_len):
        row_start = row_end
        row_end += 1 + grp_size if i_grp < reste else grp_size
        new_arr[i_grp] = np.mean(arr[row_start:row_end], axis=0)
    return new_arr



for umock in UMOCK_LIST:

    FIG_HARD = go.Figure()
    FIG_CONV = go.Figure()
    PGF_TEXT=F"bin_scale bin_nNewt number\n"

    for scalenum, scale in enumerate(SCALE_LIST):
        basename=F"./dat/guess_scale/scale_{CASE}_{umock}_"+F"{scale:3.3}"
        print(basename)
        data = pd.read_csv(basename + "_variables.csv").values
        #if data.dtype == "O":
        #    continue
        FIG_HARD.add_trace(
            go.Scatter(
                x=data[:, 0],
                y=data[:, 1],
                name=str(basename),
                mode="markers",
            )
        )
        data = pd.read_csv(basename + "_convergence.csv").values
        #data = data[data[:,1]>1]
        newd = coarsen_aray(data, NEW_LEN)
        s_axis = newd[:, 0]*0. + scale
        FIG_CONV.add_trace(
                go.Scatter(
                    x=s_axis,
                    y=(newd[:, 1]),
                    marker={"size": 20,
                            "color":'gray',# newd[:, 1],
                            "opacity": 0.05},
                    #alpha=0.8,
                    mode='markers',
                    )
                )
        SUMMARY[:,scalenum] = np.histogram(data[:,1], bins=np.arange(len(SUMMARY)+1))[0]
        PGF_TEXT += "\n"
        for newt, val in enumerate(SUMMARY[:,scalenum]):
            PGF_TEXT += F"{scale} {newt} {int(val)}\n"

    FIG_CONV.update_layout(
        title=dict(
            text=F"({umock}_{CASE})",
            font=dict(size=50),
            automargin=True,
            yref='paper',
            ),
        xaxis_title=dict( font=dict(size=30), text="Initial guess scale",),
        yaxis_title=dict( font=dict(size=30), text="n_newt",),
    )
    FIG_CONV.show()
    go.Figure(data=[go.Surface(z=np.log(SUMMARY+1))]).show()
    #FIG_HARD.show()

    ## # PGFPLOTS
    ## np.savetxt(
    ##         F'dat/PGF_DATA/guesscale_{umock}.txt',
    ##         SUMMARY,
    ##         fmt='%5d',
    ##         header=F"scale from {np.min(SCALE_LIST)} to {np.max(SCALE_LIST)}",
    ##         )
    
    with open(F'dat/PGF_DATA/guesscale_{umock}.txt', "w", encoding='utf8') as text_file:
        text_file.write(PGF_TEXT)
