# Run tension with different step sizes, then compare

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
cd ..


mkdir -p dat

casename="baushearger"

csv_args=""

cd umocks

#for umockname in *.F; do
for umockname in Holmedal_hs.F; do
 for INCS in  $(cat ../analysis/increment_and_residual_ISIZES.txt) ; do
  for RESS in $(cat ../analysis/increment_and_residual_PSITOLS.txt) ; do
   filename="ten_U${umockname:0:4}_I${INCS}_R${RESS}"
   if [ -f "../dat/irsizes/${filename}.log" ]; then
    echo "(EXISTS) $filename -DINC_SIZE=$INCS -DRES_SIZE=$RESS "
   else
    echo $filename -DINC_SIZE=$INCS -DRES_SIZE=$RESS 
    gfortran -cpp -DLS_MAX=0                     -DINC_SIZE=$INCS -DRES_SIZE=$RESS ../cases/${casename}.F $umockname
    ./a.out > ../dat/irsizes/${filename}.log
    cat ../dat/irsizes/${filename}.log | grep "MAIN"        | cut -c7-  > ../dat/irsizes/g0s0/${filename}_variables.csv
    cat ../dat/irsizes/${filename}.log | grep "CONVERGENCE" | cut -c14- > ../dat/irsizes/g0s0/${filename}_convergence.csv

    gfortran -cpp -DLS_MAX=100                    -DINC_SIZE=$INCS -DRES_SIZE=$RESS ../cases/${casename}.F $umockname
    ./a.out > ../dat/irsizes/${filename}.log
    cat ../dat/irsizes/${filename}.log | grep "MAIN"        | cut -c7-  > ../dat/irsizes/g0s1/${filename}_variables.csv
    cat ../dat/irsizes/${filename}.log | grep "CONVERGENCE" | cut -c14- > ../dat/irsizes/g0s1/${filename}_convergence.csv

    gfortran -cpp -DLS_MAX=0   -DBEST_FIRST_GUESS -DINC_SIZE=$INCS -DRES_SIZE=$RESS ../cases/${casename}.F $umockname
    ./a.out > ../dat/irsizes/${filename}.log
    cat ../dat/irsizes/${filename}.log | grep "MAIN"        | cut -c7-  > ../dat/irsizes/g1s0/${filename}_variables.csv
    cat ../dat/irsizes/${filename}.log | grep "CONVERGENCE" | cut -c14- > ../dat/irsizes/g1s0/${filename}_convergence.csv

    gfortran -cpp -DLS_MAX=100 -DBEST_FIRST_GUESS -DINC_SIZE=$INCS -DRES_SIZE=$RESS ../cases/${casename}.F $umockname
    ./a.out > ../dat/irsizes/${filename}.log
    cat ../dat/irsizes/${filename}.log | grep "MAIN"        | cut -c7-  > ../dat/irsizes/g1s1/${filename}_variables.csv
    cat ../dat/irsizes/${filename}.log | grep "CONVERGENCE" | cut -c14- > ../dat/irsizes/g1s1/${filename}_convergence.csv
   fi
   #csv_args=$csv_args" dat/irsizes/${filename}_convergence.csv"
   csv_args=$csv_args" dat/irsizes/${filename}_variables.csv"
  done
 done
done

cd ..
#python analysis/csv_plot.py 0 1 $csv_args
